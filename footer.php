

    <!-- Footer -->
    <footer class="site-footer">
      <div class="site-footer-legal">© 2018 <a href="http://themeforest.net/item/remark-responsive-bootstrap-admin-template/11989202">Remark</a></div>
      <div class="site-footer-right">
        Crafted with <i class="red-600 wb wb-heart"></i> by <a href="https://themeforest.net/user/creation-studio">Creation Studio</a>
      </div>
    </footer>
    <!-- Core  -->
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/jquery/jquery.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/animsition/animsition.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
    
    <!-- Plugins -->
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/switchery/switchery.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/intro-js/intro.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/screenfull/screenfull.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/skycons/skycons.js"></script>
        <!-- <script src="<?php echo $root_dir; ?>/remark/global/vendor/chartist/chartist.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js"></script> -->
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/aspieprogress/jquery-asPieProgress.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/jvectormap/jquery-jvectormap.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/jvectormap/maps/jquery-jvectormap-au-mill-en.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        
        <!-- Dashboard -->
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/raphael/raphael.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/morris/morris.min.js"></script>
        
        <?php if (strpos( $_SERVER['REQUEST_URI'] , 'import') !== false) { ?>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/formvalidation/formValidation.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/formvalidation/framework/bootstrap.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/matchheight/jquery.matchHeight-min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/jquery-wizard/jquery-wizard.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/jquery-ui/jquery-ui.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/blueimp-tmpl/tmpl.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/blueimp-load-image/load-image.all.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/blueimp-file-upload/jquery.fileupload.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/blueimp-file-upload/jquery.fileupload-process.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/blueimp-file-upload/jquery.fileupload-audio.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/blueimp-file-upload/jquery.fileupload-video.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/blueimp-file-upload/jquery.fileupload-validate.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/blueimp-file-upload/jquery.fileupload-ui.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/dropify/dropify.min.js"></script>
        <!-- Sortable & Nestable -->
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/sortable/Sortable.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/nestable/jquery.nestable.js"></script>
        <?php } ?>

        <?php if (strpos( $_SERVER['REQUEST_URI'] , 'add') !== false || strpos( $_SERVER['REQUEST_URI'] , 'edit') !== false) { ?>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/select2/select2.full.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/bootstrap-select/bootstrap-select.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/icheck/icheck.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/switchery/switchery.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/asrange/jquery-asRange.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/ionrangeslider/ion.rangeSlider.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/asspinner/jquery-asSpinner.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/clockpicker/bootstrap-clockpicker.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/ascolor/jquery-asColor.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/asgradient/jquery-asGradient.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/ascolorpicker/jquery-asColorPicker.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/bootstrap-maxlength/bootstrap-maxlength.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/jquery-knob/jquery.knob.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/bootstrap-touchspin/bootstrap-touchspin.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/jquery-labelauty/jquery-labelauty.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/timepicker/jquery.timepicker.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/datepair/datepair.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/datepair/jquery.datepair.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/jquery-strength/password_strength.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/jquery-strength/jquery-strength.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/multi-select/jquery.multi-select.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/typeahead-js/bloodhound.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/typeahead-js/typeahead.jquery.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
        <!-- Editable Table -->
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/editable-table/mindmup-editabletable.js"></script>
        <?php } ?>
        
        <?php if (strpos( $_SERVER['REQUEST_URI'] , 'view') !== false) { ?>
        <!-- FooTable -->
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/moment/moment.min.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/vendor/footable/footable.min.js"></script>
        <?php } ?>
    
    <!-- Scripts -->
    <script src="<?php echo $root_dir; ?>/remark/global/js/Component.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/js/Base.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/js/Config.js"></script>
    
    <script src="<?php echo $root_dir; ?>/remark/base/assets/js/Section/Menubar.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/base/assets/js/Section/GridMenu.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/base/assets/js/Section/Sidebar.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/base/assets/js/Section/PageAside.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/base/assets/js/Plugin/menu.js"></script>
    
    <script src="<?php echo $root_dir; ?>/remark/global/js/config/colors.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/base/assets/js/config/tour.js"></script>
    <script>Config.set('assets', '<?php echo $root_dir; ?>/remark/base/assets');</script>
    
    <!-- Page -->
    <script src="<?php echo $root_dir; ?>/remark/base/assets/js/Site.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/asscrollable.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/slidepanel.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/switchery.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/matchheight.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/jvectormap.js"></script>

        <?php if (strpos( $_SERVER['REQUEST_URI'] , 'import') !== false) { ?>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/jquery-wizard.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/matchheight.js"></script>
        <?php } ?>

        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/dropify.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/jquery-placeholder.js"></script>

        <?php if (strpos( $_SERVER['REQUEST_URI'] , 'add') !== false || strpos( $_SERVER['REQUEST_URI'] , 'edit') !== false) { ?>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/select2.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/bootstrap-tokenfield.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/bootstrap-tagsinput.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/bootstrap-select.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/icheck.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/switchery.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/asrange.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/ionrangeslider.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/asspinner.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/clockpicker.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/ascolorpicker.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/bootstrap-maxlength.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/jquery-knob.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/bootstrap-touchspin.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/card.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/jquery-labelauty.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/bootstrap-datepicker.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/jt-timepicker.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/datepair.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/jquery-strength.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/multi-select.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/jquery-placeholder.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/base/assets/examples/js/forms/advanced.js"></script>
        <!-- Editable Table -->
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/editable-table.js"></script>
        <?php } ?>
    
        <script src="<?php echo $root_dir; ?>/remark/base/assets/examples/js/dashboard/v1.js"></script>
        <!-- Editable Table -->
        <script src="<?php echo $root_dir; ?>/remark/base/assets/examples/js/tables/editable.js"></script>

        <?php if (strpos( $_SERVER['REQUEST_URI'] , 'import') !== false) { ?>
        <!-- Wizard -->
        <script src="<?php echo $root_dir; ?>/remark/base/assets/examples/js/forms/wizard.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/base/assets/examples/js/forms/uploads.js"></script>
        <!-- Sortable & Nestable -->
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/sortable.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/nestable.js"></script>
        <script src="<?php echo $root_dir; ?>/remark/global/js/Plugin/tasklist.js"></script>
        <?php } ?>

        <?php if (strpos( $_SERVER['REQUEST_URI'] , 'view') !== false) { ?>
        <!-- Footable -->
        <script src="<?php echo $root_dir; ?>/remark/base/assets/examples/js/tables/footable.js"></script>
        <?php } ?>

        <?php
        $sql = "SELECT year FROM nutrition GROUP BY year";
        $result = mysqli_query($conn, $sql);
        $string = "";
        if (mysqli_num_rows($result) > 0) { while($row = mysqli_fetch_assoc($result)) {
          $bmi_categories = array("uw"=>"Underweight","n"=>"Normal","ow"=>"Overweight","o"=>"Obese");
          foreach ( $bmi_categories as $key => $value ) {
            $result2 = mysqli_query($conn, "SELECT COUNT(id) AS category FROM nutrition WHERE bmi_category='" . $value . "' AND year=" . $row["year"]);
            if (mysqli_num_rows($result2) > 0) { while($row2 = mysqli_fetch_assoc($result2)) { $bmi_categories[$key] = $row2['category']; } }
          }
          $string .= '{
            "y": "' . $row["year"] . '",
            "uw": ' . ($bmi_categories['uw'] === 'Underweight' ? 0 : $bmi_categories['uw']) . ',
            "n": ' . ($bmi_categories['n'] === 'Normal' ? 0 : $bmi_categories['n']) . ',
            "ow": ' . ($bmi_categories['ow'] === 'Overweight' ? 0 : $bmi_categories['ow']) . ',
            "o": ' . ($bmi_categories['o'] === 'Obese' ? 0 : $bmi_categories['o']) . '
          }, ';
        } }
        ?>
        
        <!-- Dashboard -->
        <script>
          (function () {
            Morris.Line({
              element: 'exampleMorrisLine',
              data: [
                <?php echo $string; ?>
              ],
              xkey: 'y',
              ykeys: ['uw', 'n', 'ow', 'o'],
              labels: ['Underweight', 'Normal', 'Overweight', 'Obese'],
              resize: true,
              pointSize: 3,
              smooth: true,
              gridTextColor: '#474e54',
              gridLineColor: '#eef0f2',
              goalLineColors: '#e3e6ea',
              gridTextFamily: Config.get('fontFamily'),
              gridTextWeight: '300',
              numLines: 10,
              gridtextSize: 14,
              lineWidth: 1,
              lineColors: [Config.colors("primary", 700), Config.colors("green", 600), Config.colors("teal", 600), Config.colors("grey", 600)]
            });
          })();
        </script>
    
    <!-- Custom Javascript -->
    <script src="<?php echo $root_dir; ?>/assets/custom.js"></script>
  </body>
</html>

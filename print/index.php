<?php include("../config.php"); ?>
<?php include("../db_connection.php"); ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="">
    
    <title>Print | Paco Catholic School - Student Health Records Management System</title>
    
    <link rel="apple-touch-icon" href="<?php echo $root_dir; ?>/remark/base/assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="<?php echo $root_dir; ?>/remark/base/assets/images/favicon.ico">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $root_dir; ?>/assets/favicon/apple-touch-icon.png">
    <link rel="shortcut icon" href="<?php echo $root_dir; ?>/assets/favicon/favicon.ico">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $root_dir; ?>/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $root_dir; ?>/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $root_dir; ?>/assets/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo $root_dir; ?>/assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="theme-color" content="#ffffff">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/base/assets/css/site.min.css">
    
    <!-- Fonts -->
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/fonts/weather-icons/weather-icons.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/fonts/web-icons/web-icons.min.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/assets/custom.css">
    
    <!--[if lt IE 9]>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/media-match/media.match.min.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
    Breakpoints();
    </script>
</head>
<body>
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/css/bootstrap.min.css">
    <style>
        body { padding: 0; }
        /* body { display: none; } */
        .table thead th, .table thead td { padding: .3rem; font-weight: bold; }
        .table tbody th, .table tbody td { padding: .1rem; }
        .table-bordered thead td, .table-bordered thead th { border-bottom-width: 0; }
        @media print {
            @page { margin: 0; }
            body { display: block; margin: 0.5in; }
        }
        .container { max-width: 7.5in !important; }
        .print-page {  }
        .print-page * { font-family: 'Times New Roman'; color: black; }
        .print-page .header { display: flex; justify-content: center; }
        .print-page .header .header-img { width: 0; }
        .print-page .header .header-img .logo { width: 95px; position: relative; right: 110px; }
        .print-page .header .header-title { display: flex; flex-direction: column; align-items: center; justify-content: center; }
        .print-page .header .header-title .heading { font-size: 24px; font-weight: bold; margin-bottom: 0; }
        .print-page .header .header-title .subheading { font-family: 'Arial'; font-size: 16px; }
        .print-page .title { font-size: 20px; }
        .print-page .personal-data { max-width: 8.5in; margin: 0 auto; }
        .print-page .underline { display: flex; justify-content: center; border-bottom: 1px solid black; font-family: Arial; font-weight: bold; }
        .print-page .label { float: left; position: relative; bottom: -6px; margin-right: 6px; }
        .print-page .invisible { visibility: hidden; color: transparent; }
    </style>

    <script type="text/javascript">window.print();</script>

    <!-- Page -->
    <div>
        <?php
        $query = $_GET;
        $sql = "SELECT * FROM `student` WHERE id=" . $query['id'];
        $result = mysqli_query($conn, $sql);

        function testValue($value) {
            return ($value !== 'undefined' && $value !== '0' && $value !== 'null' && $value !== '' && $value !== ' ' && $value !== null) ? $value : '&nbsp;';
        }

        if (mysqli_num_rows($result) > 0) { while($row = mysqli_fetch_assoc($result)) { ?>
        <div class="print-page container">
            <div class="header">
                <div class="header-img">
                    <img src="../assets/pacocs-logo.png" alt="paco-catholic-school-logo" class="logo">
                </div>
                <div class="header-title">
                    <h1 class="heading">PACO CATHOLIC SCHOOL</h1>
                    <h3 class="subheading">1521 Paz Street, Paco, Manila 1007</h3>
                </div>
            </div>
            <h2 class="title text-center">STUDENT HEALTH RECORD</h2>
            <div class="personal-data mt-5">
                <div class="row mt-2">
                    <div class="col-9">
                        <div class="label">NAME</div>
                        <div class="row">
                            <div class="col-4 text-center underline text-uppercase"><?php echo testValue($row["name_last"]); ?></div>
                            <div class="col-4 text-center underline text-uppercase"><?php echo testValue($row["name_first"]); ?></div>
                            <div class="col-4 text-center underline text-uppercase"><?php echo testValue($row["name_middle"]); ?></div>
                        </div>
                        <div class="label invisible">NAME</div>
                        <div class="row">
                            <div class="col-4 text-center"><em>Last</em></div>
                            <div class="col-4 text-center"><em>First</em></div>
                            <div class="col-4 text-center"><em>Middle</em></div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="label">CLASS</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["gys"]); ?></div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-2">
                        <div class="label">SEX</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["gender"]); ?></div>
                    </div>
                    <div class="col-2">
                        <div class="label">AGE</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["age"]); ?></div>
                    </div>
                    <div class="col-4">
                        <div class="label">DATE OF BIRTH</div>
                        <div class="text-center underline text-uppercase"><?php echo (testValue($row["birth_date"]) == $row["birth_date"]) ? date( 'M d, Y', strtotime($row["birth_date"]) ) : '&nbsp'; ?></div>
                    </div>
                    <div class="col-4">
                        <div class="label">NATIONALITY</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["nationality"]); ?></div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-4">
                        <div class="label">CONTACT NO.</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["contact_no"]); ?></div>
                    </div>
                    <div class="col-8">
                        <div class="label">ADDRESS</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["address"]); ?></div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12">
                        <small>Parent/Guardian</small>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="label">FULL NAME</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["guardian_name"]); ?></div>
                    </div>
                    <div class="col-4">
                        <div class="label">CONTACT NO.</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["guardian_contact_no"]); ?></div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-12">
                        <div class="label">ADDRESS</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["guardian_address"]); ?></div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-5">
                        <div class="label">OFFICE CONTACT NO.</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["guardian_office_contact_no"]); ?></div>
                    </div>
                    <div class="col-7">
                        <div class="label">OFFICE ADDRESS</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["guardian_office_address"]); ?></div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12">
                        <small>Alternate person to be notified in case of emergency</small>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="label">FULL NAME</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["alternate_person_name"]); ?></div>
                    </div>
                    <div class="col-4">
                        <div class="label">CONTACT NO.</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["alternate_person_contact_no"]); ?></div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-12">
                        <small>Elder sibling(s) in school</small>
                    </div>
                </div>
                <div class="row">
                    <div class="col-8">
                        <div class="label">FULL NAME</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["sibling_1_name"]); ?></div>
                    </div>
                    <div class="col-4">
                        <div class="label">CLASS</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["sibling_1_cys"]); ?></div>
                    </div>
                </div>
                <?php if (testValue($row["sibling_2_name"]) == $row["sibling_2_name"]) { ?>
                <div class="row">
                    <div class="col-8">
                        <div class="label">FULL NAME</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["sibling_2_name"]); ?></div>
                    </div>
                    <div class="col-4">
                        <div class="label">CLASS</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["sibling_2_cys"]); ?></div>
                    </div>
                </div>
                <?php } if (testValue($row["sibling_3_name"]) == $row["sibling_3_name"]) { ?>
                <div class="row">
                    <div class="col-8">
                        <div class="label">FULL NAME</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["sibling_3_name"]); ?></div>
                    </div>
                    <div class="col-4">
                        <div class="label">CLASS</div>
                        <div class="text-center underline text-uppercase"><?php echo testValue($row["sibling_3_cys"]); ?></div>
                    </div>
                </div>
                <?php } ?>
            </div>
        <?php } } ?>
            <div class="medicines mt-5">
                <p>List of medicines that can be administered as per the student's parent or guardian: </p>
                <div class="row">
                    <?php
                    $sql2 = "SELECT * FROM `medicine` WHERE student_id=" . $query['id'];
                    $result2 = mysqli_query($conn, $sql2);

                    if (mysqli_num_rows($result2) > 0) { while($row2 = mysqli_fetch_assoc($result2)) { ?>
                    <!-- <?php echo '<pre>'; var_dump($row2["can_be_administered"]); echo '</pre>'; ?> -->
                    <div class="col-3"><?php echo ($row2["can_be_administered"] == '1') ? '&#10004;' : '&#10008;'; echo ' ' . $row2["name"]; ?></div>
                    <?php } } else { ?>
                    <div class="col-12">No records.</div>
                    <?php } ?>
                </div>
            </div>
            <div class="medicines mt-5">
                <p>Nutrition records: </p>
                <table class="table table-bordered text-center">
                    <thead>
                        <tr>
                            <th>Year</th>
                            <th>Weight</th>
                            <th>Height</th>
                            <th>BMI</th>
                            <th>Category</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql3 = "SELECT * FROM `nutrition` WHERE student_id=" . $query['id'] . " ORDER BY year DESC";
                        $result3 = mysqli_query($conn, $sql3);
                        if (mysqli_num_rows($result3) > 0) { while($row3 = mysqli_fetch_assoc($result3)) { ?>
                        <tr>
                            <td><?php echo $row3['year']; ?></td>
                            <td><?php echo $row3['weight']; ?></td>
                            <td><?php echo $row3['height']; ?></td>
                            <td><?php echo $row3['bmi']; ?></td>
                            <td><?php echo $row3['bmi_category']; ?></td>
                        </tr>
                        <?php } } else { ?>
                        <tr>
                            <td class="text-center" colspan="12">No records.</td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- End Page -->
    
</body>
</html>

<?php mysqli_close($conn); ?>
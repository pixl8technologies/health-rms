
<?php
include("../config.php");
include("../db_connection.php");

$sql_error = "";
$student_data = $_POST["studentData"];
foreach ($student_data as $value) {
    $sql = "INSERT INTO `student`(
        `name_last`,
        `name_first`,
        `name_middle`,
        `gys`,
        `gender`,
        `birth_date`,
        `age`,
        `nationality`,
        `address`,
        `contact_no`,
        `guardian_name`,
        `guardian_contact_no`,
        `guardian_address`,
        `guardian_office_contact_no`,
        `guardian_office_address`,
        `alternate_person_name`,
        `alternate_person_contact_no`,
        `sibling_1_name`,
        `sibling_1_cys`,
        `sibling_2_name`,
        `sibling_2_cys`,
        `sibling_3_name`,
        `sibling_3_cys`) VALUES ";
    // echo '<pre>Foreach data: ';
    // var_dump($value);
    // echo '</pre>';
    $birth_date = strtotime($value["'birth_date'"]);
    $birth_date = date('Y-m-d H:i:s', $birth_date);
    $sql .= "(
        '" . $value["'name_last'"] . "',
        '" . $value["'name_first'"] . "',
        '" . $value["'name_middle'"] . "',
        '" . $value["'gys'"] . "',
        '" . $value["'gender'"] . "',
        '" . $birth_date . "',
        '" . $value["'age'"] . "',
        '" . $value["'nationality'"] . "',
        '" . $value["'address'"] . "',
        '" . $value["'contact_no'"] . "',
        '" . $value["'guardian_name'"] . "',
        '" . $value["'guardian_contact_no'"] . "',
        '" . $value["'guardian_address'"] . "',
        '" . $value["'guardian_office_contact_no'"] . "',
        '" . $value["'guardian_office_address'"] . "',
        '" . $value["'alternate_person_name'"] . "',
        '" . $value["'alternate_person_contact_no'"] . "',
        '" . $value["'sibling_1_name'"] . "',
        '" . $value["'sibling_1_cys'"] . "',
        '" . $value["'sibling_2_name'"] . "',
        '" . $value["'sibling_2_cys'"] . "',
        '" . $value["'sibling_3_name'"] . "',
        '" . $value["'sibling_3_cys'"] . "'
    )";
    if ( !(mysqli_query($conn, $sql)) ) $sql_error .= mysqli_error($conn) . ". ";
    $student_id = mysqli_insert_id($conn);

    // Create a query to add medicine data
    $sql = "INSERT INTO `medicine`( `name`, `can_be_administered`, `student_id` ) VALUES ";
    foreach ($value as $medicine_key => $medicine_value) {
        if (strpos($medicine_key, "'medicine") === 0) {
            $sql .= "('" . ucwords(str_replace('_', ' ', substr($medicine_key, 10)))  . ", " . (($medicine_value == 'on' || $medicine_value == 'true') ? 1 : 0) . ", " . $student_id . "), ";
        }
    }
    $sql = substr( $sql, 0, (strlen($sql) - 2) );
    // Insert medicine data to database
    if ( !(mysqli_query($conn, $sql)) ) {
        $sql_error .= mysqli_error($conn) . ". ";
    }

/*
    // Create a query to add nutrition data
    $sql = "INSERT INTO `nutrition`( `year`, `bmi`, `bmi_cateory`, `weight`, `height`, `student_id` ) VALUES ";
    foreach ($_POST as $key => $value) { 
        if ( strpos($key, 'nutrition_') === 0 ) {
            if ( strpos($key, 'year') !== false ) $sql .= "(" . $value . ", ";
            else if ( strpos($key, 'bmi') !== false ) $sql .= $value . ", ";
            else if ( strpos($key, 'category') !== false ) $sql .= "'" . $value . "', ";
            else if ( strpos($key, 'weight') !== false ) $sql .= $value . ", ";
            else if ( strpos($key, 'height') !== false ) $sql .= $value . ", " . $student_id . "), ";
        }
    }
    $sql = substr( $sql, 0, (strlen($sql) - 2) );
    // Insert nutrition data to database
    if ( !(mysqli_query($conn, $sql)) ) {
        $sql_success = false;
        $sql_error .= mysqli_error($conn) . ". ";
    }
*/

}
mysqli_close($conn); ?>
<p><?php echo $sql_error; ?></p>

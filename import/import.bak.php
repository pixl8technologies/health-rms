
<?php
$sql_error = "";
$birth_date = strtotime($_POST["birth_date"]);
$birth_date = date('Y-m-d H:i:s', $birth_date);
$sql = "INSERT INTO `student`(
    `name_last`,
    `name_first`,
    `name_middle`,
    `gender`,
    `birth_date`,
    `age`,
    `nationality`,
    `address`,
    `contact_no`,
    `guardian_name`,
    `guardian_contact_no`,
    `guardian_address`,
    `guardian_office_contact_no`,
    `guardian_office_address`,
    `alternate_person_name`,
    `alternate_person_contact_no`,
    `sibling_1_name`,
    `sibling_1_cys`,
    `sibling_2_name`,
    `sibling_2_cys`,
    `sibling_3_name`,
    `sibling_3_cys`) VALUES (
    '" . $_POST["name_last"] . "',
    '" . $_POST["name_first"] . "',
    '" . $_POST["name_middle"] . "',
    '" . $_POST["gender"] . "',
    '" . $birth_date . "',
    '" . $_POST["age"] . "',
    '" . $_POST["nationality"] . "',
    '" . $_POST["address"] . "',
    '" . $_POST["contact_no"] . "',
    '" . $_POST["guardian_name"] . "',
    '" . $_POST["guardian_contact_no"] . "',
    '" . $_POST["guardian_address"] . "',
    '" . $_POST["guardian_office_contact_no"] . "',
    '" . $_POST["guardian_office_address"] . "',
    '" . $_POST["alternate_person_name"] . "',
    '" . $_POST["alternate_person_contact_no"] . "',
    '" . $_POST["sibling_1_name"] . "',
    '" . $_POST["sibling_1_cys"] . "',
    '" . $_POST["sibling_2_name"] . "',
    '" . $_POST["sibling_2_cys"] . "',
    '" . $_POST["sibling_3_name"] . "',
    '" . $_POST["sibling_3_cys"] . "'
)";

$sql_success = true;
if ( !(mysqli_query($conn, $sql)) ) {
    $sql_success = false;
    $sql_error = mysqli_error($conn) . ". ";
}
$student_id = mysqli_insert_id($conn);

// Create a query to add medicine data
$sql = "INSERT INTO `medicine`( `name`, `can_be_administered`, `student_id` ) VALUES ";
foreach ($_POST as $key => $value) { 
if (strpos($key, 'inputMedicine') === 0) {
    $sql .= "('" . $_POST["label" . $key] . "', " . ($value == 'on' ? true : false) . ", " . $student_id . "), ";
}
}
$sql = substr( $sql, 0, (strlen($sql) - 2) );
// Insert medicine data to database
if ( !(mysqli_query($conn, $sql)) ) {
    $sql_success = false;
    $sql_error .= mysqli_error($conn) . ". ";
}

// Create a query to add nutrition data
$sql = "INSERT INTO `nutrition`( `year`, `bmi`, `bmi_cateory`, `weight`, `height`, `student_id` ) VALUES ";
foreach ($_POST as $key => $value) { 
if ( strpos($key, 'nutrition_') === 0 ) {
    if ( strpos($key, 'year') !== false ) $sql .= "(" . $value . ", ";
    else if ( strpos($key, 'bmi') !== false ) $sql .= $value . ", ";
    else if ( strpos($key, 'category') !== false ) $sql .= "'" . $value . "', ";
    else if ( strpos($key, 'weight') !== false ) $sql .= $value . ", ";
    else if ( strpos($key, 'height') !== false ) $sql .= $value . ", " . $student_id . "), ";
}
}
$sql = substr( $sql, 0, (strlen($sql) - 2) );
// Insert nutrition data to database
if ( !(mysqli_query($conn, $sql)) ) {
    $sql_success = false;
    $sql_error .= mysqli_error($conn) . ". ";
}

mysqli_close($conn);

if ($sql_success) { ?>
<h2 class="mt-0">Successfully added.</h2>
<p>Your request has been processed.</p>
<a href="<?php echo $root_dir; ?>/add" class="btn btn-success btn-md" >Add More</a>
<?php } else { ?>
<h2 class="mt-0 text-danger">There was an error in processing your request.</h2>
<p><?php echo $sql_error; ?></p>
<a href="<?php echo $root_dir; ?>/add" class="btn btn-default btn-md" >Try Again</a>
<?php } ?>
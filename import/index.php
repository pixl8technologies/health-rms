<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page">
      <div class="page-header">
        <h1 class="page-title">Import Student Health Records from a CSV File</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item"><a href="javascript:void(0)">Student Records</a></li>
          <li class="breadcrumb-item active">Import Student Health Records from a CSV File</li>
        </ol>
        <div class="page-header-actions">
          <a class="btn btn-sm btn-icon btn-default btn-outline btn-round" href="<?php echo $root_dir; ?>/import"
            data-toggle="tooltip" data-original-title="Import">
            <i class="icon wb-upload" aria-hidden="true"></i>
          </a>
          <a class="btn btn-sm btn-icon btn-default btn-outline btn-round" href="<?php echo $root_dir; ?>/add"
            data-toggle="tooltip" data-original-title="Add">
            <i class="icon wb-plus" aria-hidden="true"></i>
          </a>
          <a class="btn btn-sm btn-icon btn-default btn-outline btn-round" href="<?php echo $root_dir; ?>/export"
            data-toggle="tooltip" data-original-title="Export">
            <i class="icon wb-download" aria-hidden="true"></i></a>
        </div>
      </div>
      
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
          <div class="col-lg-12">

            <!-- Panel Wizard Form -->
            <div class="panel" id="exampleWizardForm">
              <div class="panel-heading">
                <h3 class="panel-title">
                  Student Health Record CSV Import Wizard
                  <a href="javascript:void(0)" aria-hidden="true" data-target="#import-wizard-info" data-toggle="modal">
                    <i class="ml-5 icon wb-info-circle" aria-hidden="true"></i>
                  </a>
                </h3>
                <!-- Modal -->
                <div class="modal fade" id="import-wizard-info" aria-hidden="true" aria-labelledby="import-wizard-info" role="dialog" tabindex="-1">
                  <div class="modal-dialog modal-simple">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title">CSV Import Wizard Info</h4>
                      </div>
                      <div class="modal-body">
                        <div class="text-left">
                          <p>Use this wizard to import from a CSV file. If you need a template, download the export data
                            <a href="<?php echo $root_dir; ?>/export">here</a>. Currently, the import wizard do not support BMI export.
                            To add BMI to the records to be imported, you need to manually add it or just edit it after import.</p>
                          <p>The maximum rows in the CSV file is &lt;insert max rows here&gt; rows. You can split it into two files to
                            import it all.</p>
                          <p>If the file you have is in Microsoft&copy; Excel (.xls .xlsx), you can save it into a CSV file.
                            Just click save as then choose CSV.</p>
                          <p>The system will only add the records into the database, and not replace anything.
                            If you have same data that was added before,
                            you can delete it to prevent duplicates in the record.</p>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Got it!</button>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End Modal -->
              </div>
              <div class="panel-body">
                <!-- Steps -->
                <div class="steps steps-sm row" data-plugin="matchHeight" data-by-row="true" role="tablist">
                  <div class="step col-lg-3 current" data-target="#upload" role="tab">
                    <span class="step-number">1</span>
                    <div class="step-desc">
                      <span class="step-title">Upload</span>
                      <p>Upload a CSV File</p>
                    </div>
                  </div>

                  <div class="step col-lg-3" data-target="#map" id="wizard-header-map" role="tab">
                    <span class="step-number">2</span>
                    <div class="step-desc">
                      <span class="step-title">Column Mapping</span>
                      <p>Map CSV fields to records</p>
                    </div>
                  </div>

                  <div class="step col-lg-3" data-target="#import" role="tab">
                    <span class="step-number">3</span>
                    <div class="step-desc">
                      <span class="step-title">Import</span>
                      <p>Wait until all records are imported</p>
                    </div>
                  </div>

                  <div class="step col-lg-3" data-target="#done" role="tab">
                    <span class="step-number">4</span>
                    <div class="step-desc">
                      <span class="step-title">Done!</span>
                      <p>Student data has been added</p>
                    </div>
                  </div>
                </div>
                <!-- End Steps -->

                <!-- Wizard Content -->
                <div class="wizard-content">
                  <div class="wizard-pane active" id="upload" role="tabpanel">
                    <form id="uploadForm">
                      <p>&nbsp;</p>
                      <div class="col-md-8 mx-auto example-wrap">
                        <div class="example">
                          <input type="file" required id="input-file-now" accept=".csv" data-plugin="dropify" data-default-file=""/>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="wizard-pane" id="map" role="tabpanel">
                    <form id="mapForm">
                      <div class="error d-none">
                        <div class="row">
                          <div class="col-md-5 mx-auto my-50">
                            <div class="alert alert-danger text-center py-20">
                              <h4 class="card-title">Error in column mapping</h4>
                              <p class="card-text">You need to select a CSV file first. Go back to prevoius step.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="success">
                        <div class="row">
                          <div class="col-md-7 mx-auto text-center my-20">
                            <h3 class="example-title">Matching the Fields</h3>
                            <p>Move the column header names on the right to match the columns headers
                              of your CSV file on the left. If there is a column in your csv file
                              that has no match in the system's header, delete it before uploading.</p>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-6 text-right">
                            <h5>Headers of your CSV file</h5>
                          </div>
                          <div class="col-6">
                            <h5>Headers of the Health RMS</h5>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-6">
                            <div class="m-lg-0">
                              <ul class="list-group list-group-full text-right" id="csv-column-heads"></ul>
                            </div>
                          </div>
                          <div class="col-6">
                            <!-- Example Sortable -->
                            <div class="example-wrap m-lg-0">
                              <ul class="list-group sortable list-group-full" data-plugin="sortable" id="database-column-heads">
                                <li class="list-group-item" data-key="name_last">
                                  <i class="icon wb-move" aria-hidden="true"></i>Last Name
                                </li>
                                <li class="list-group-item" data-key="name_first">
                                  <i class="icon wb-move" aria-hidden="true"></i>First Name
                                </li>
                                <li class="list-group-item" data-key="name_middle">
                                  <i class="icon wb-move" aria-hidden="true"></i>Middle Name
                                </li>
                                <li class="list-group-item" data-key="gys">
                                  <i class="icon wb-move" aria-hidden="true"></i>Grade/Section
                                </li>
                                <li class="list-group-item" data-key="gender">
                                  <i class="icon wb-move" aria-hidden="true"></i>Gender
                                </li>
                                <li class="list-group-item" data-key="birth_date">
                                  <i class="icon wb-move" aria-hidden="true"></i>Date of Birth
                                </li>
                                <li class="list-group-item" data-key="age">
                                  <i class="icon wb-move" aria-hidden="true"></i>Age
                                </li>
                                <li class="list-group-item" data-key="nationality">
                                  <i class="icon wb-move" aria-hidden="true"></i>Nationality
                                </li>
                                <li class="list-group-item" data-key="address">
                                  <i class="icon wb-move" aria-hidden="true"></i>Address
                                </li>
                                <li class="list-group-item" data-key="contact_no">
                                  <i class="icon wb-move" aria-hidden="true"></i>Contact Number
                                </li>
                                <li class="list-group-item" data-key="guardian_name">
                                  <i class="icon wb-move" aria-hidden="true"></i>Parent/Guardian Full Name
                                </li>
                                <li class="list-group-item" data-key="guardian_contact_no">
                                  <i class="icon wb-move" aria-hidden="true"></i>Parent/Guardian Contact Number
                                </li>
                                <li class="list-group-item" data-key="guardian_address">
                                  <i class="icon wb-move" aria-hidden="true"></i>Parent/Guardian Address
                                </li>
                                <li class="list-group-item" data-key="guardian_office_contact_no">
                                  <i class="icon wb-move" aria-hidden="true"></i>Parent/Guardian Office Contact Number
                                </li>
                                <li class="list-group-item" data-key="guardian_office_address">
                                  <i class="icon wb-move" aria-hidden="true"></i>Parent/Guardian Office Address
                                </li>
                                <li class="list-group-item" data-key="alternate_person_name">
                                  <i class="icon wb-move" aria-hidden="true"></i>Alternate Person to Call Full Name
                                </li>
                                <li class="list-group-item" data-key="alternate_person_contact_no">
                                  <i class="icon wb-move" aria-hidden="true"></i>Alternate Person to Call Contact Number
                                </li>
                                <li class="list-group-item" data-key="sibling_1_name">
                                  <i class="icon wb-move" aria-hidden="true"></i>Elder Siblings in School Full Name
                                </li>
                                <li class="list-group-item" data-key="sibling_1_cys">
                                  <i class="icon wb-move" aria-hidden="true"></i>Elder Siblings in School Grade/Year/Section
                                </li>
                                <li class="list-group-item" data-key="sibling_2_name">
                                  <i class="icon wb-move" aria-hidden="true"></i>Elder Siblings in School Full Name
                                </li>
                                <li class="list-group-item" data-key="sibling_2_cys">
                                  <i class="icon wb-move" aria-hidden="true"></i>Elder Siblings in School Grade/Year/Section
                                </li>
                                <li class="list-group-item" data-key="sibling_3_name">
                                  <i class="icon wb-move" aria-hidden="true"></i>Elder Siblings in School Full Name
                                </li>
                                <li class="list-group-item" data-key="sibling_3_cys">
                                  <i class="icon wb-move" aria-hidden="true"></i>Elder Siblings in School Grade/Year/Section
                                </li>
                                <li class="list-group-item" data-key="medicine_paracetamol">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Paracetamol
                                </li>
                                <li class="list-group-item" data-key="medicine_mefenamic_acid">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Mefenamic Acid
                                </li>
                                <li class="list-group-item" data-key="medicine_bonamine">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Bonamine
                                </li>
                                <li class="list-group-item" data-key="medicine_kremil_s">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Kremil S.
                                </li>
                                <li class="list-group-item" data-key="medicine_vicks">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Vicks
                                </li>
                                <li class="list-group-item" data-key="medicine_white_flower">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: White Flower
                                </li>
                                <li class="list-group-item" data-key="medicine_efficacent_oil">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Efficacent Oil
                                </li>
                                <li class="list-group-item" data-key="medicine_hydrite">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Hydrite
                                </li>
                                <li class="list-group-item" data-key="medicine_neozep">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Neozep
                                </li>
                                <li class="list-group-item" data-key="medicine_carbocistein">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Carbocistein
                                </li>
                                <li class="list-group-item" data-key="medicine_salbutanol">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Salbutanol
                                </li>
                                <li class="list-group-item" data-key="medicine_loratidine">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Loratidine
                                </li>
                                <li class="list-group-item" data-key="medicine_desowen">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Desowen
                                </li>
                                <li class="list-group-item" data-key="medicine_betadine">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Betadine
                                </li>
                                <li class="list-group-item" data-key="medicine_visine">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Visine
                                </li>
                                <li class="list-group-item" data-key="medicine_bactidol">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Bactidol
                                </li>
                                <li class="list-group-item" data-key="medicine_bsi">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: BSI
                                </li>
                                <li class="list-group-item" data-key="medicine_flammazine_cream">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Flammazine Cream
                                </li>
                                <li class="list-group-item" data-key="medicine_cold_compress">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Cold Compress
                                </li>
                                <li class="list-group-item" data-key="medicine_deguadin">
                                  <i class="icon wb-move" aria-hidden="true"></i>Medicine that can be administered: Deguadin
                                </li>
                              </ul>
                            </div>
                            <!-- End Example Sortable -->
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="wizard-pane" id="import" role="tabpanel">
                    <form id="importForm">
                      <div class="example-loading h-150 vertical-align text-center">
                        <div class="loader vertical-align-middle loader-default"></div>
                      </div>
                    </form>
                    <div id="import-sql"></div>
                    <div id="import-ajax"></div>
                  </div>
                  <div class="wizard-pane" id="done" role="tabpanel">
                    <div class="text-center my-20">
                      <i class="icon wb-check font-size-40" aria-hidden="true"></i>
                      <h4>Student data has been imported succesfully.</h4>
                    </div>
                  </div>
                </div>
                <!-- End Wizard Content -->

              </div>
            </div>
            <!-- End Panel Wizard One Form -->

          </div>
        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>
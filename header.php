<?php
session_start();
error_reporting(E_ALL &~ E_NOTICE);
  
include("config.php");
include("db_connection.php");

if ( isset( $_SESSION['u_id'] ) ) {
  $userId = $_SESSION['u_id'];
  $userName = $_SESSION['username'];
} else {
  header("Location: " . $root_dir . "/log-in?notloggedin");
}
$logDir = $root_dir . "/includes/logout.php";
?>

<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    
    <title>Dashboard | Remark Admin Template</title>
    
    <link rel="apple-touch-icon" href="<?php echo $root_dir; ?>/remark/base/assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="<?php echo $root_dir; ?>/remark/base/assets/images/favicon.ico">

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $root_dir; ?>/assets/favicon/apple-touch-icon.png">
    <link rel="shortcut icon" href="<?php echo $root_dir; ?>/assets/favicon/favicon.ico">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $root_dir; ?>/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $root_dir; ?>/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $root_dir; ?>/assets/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo $root_dir; ?>/assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#2b5797">
    <meta name="theme-color" content="#ffffff">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/base/assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/flag-icon-css/flag-icon.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/chartist/chartist.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/jvectormap/jquery-jvectormap.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">

        <!-- Dashboard -->
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/morris/morris.css">

        <?php if (strpos( $_SERVER['REQUEST_URI'] , 'import') !== false) { ?>
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/jquery-wizard/jquery-wizard.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/formvalidation/formValidation.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/blueimp-file-upload/jquery.fileupload.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/dropify/dropify.css">
        <!-- Sortable & Nestable -->
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/nestable/nestable.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/tasklist/tasklist.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/sortable/sortable.css">
        <?php } ?>
        
        <?php if (strpos( $_SERVER['REQUEST_URI'] , 'add') !== false || strpos( $_SERVER['REQUEST_URI'] , 'edit') !== false) { ?>
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/base/assets/examples/css/forms/layouts.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/select2/select2.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/bootstrap-select/bootstrap-select.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/icheck/icheck.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/switchery/switchery.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/asrange/asRange.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/ionrangeslider/ionrangeslider.min.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/asspinner/asSpinner.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/clockpicker/clockpicker.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/ascolorpicker/asColorPicker.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/bootstrap-touchspin/bootstrap-touchspin.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/jquery-labelauty/jquery-labelauty.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/bootstrap-maxlength/bootstrap-maxlength.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/timepicker/jquery-timepicker.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/jquery-strength/jquery-strength.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/multi-select/multi-select.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/typeahead-js/typeahead.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/base/assets/examples/css/forms/advanced.css">
        <!-- Editable Table -->
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/editable-table/editable-table.css">
        <?php } ?>
        
        <?php if (strpos( $_SERVER['REQUEST_URI'] , 'view') !== false) { ?>
        <!-- FooTable -->
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/vendor/footable/footable.core.css">
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/base/assets/examples/css/tables/footable.css">
        <?php } ?>

        <!-- For Modals -->
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/base/assets/examples/css/uikit/modals.css">
    
    <!-- Fonts -->
        <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/fonts/weather-icons/weather-icons.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/fonts/web-icons/web-icons.min.css">
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/remark/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo $root_dir; ?>/assets/custom.css">
    
    <!--[if lt IE 9]>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/media-match/media.match.min.js"></script>
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="<?php echo $root_dir; ?>/remark/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition dashboard">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
    
      <div class="navbar-header">
        <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
          data-toggle="menubar">
          <span class="sr-only">Toggle navigation</span>
          <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
          data-toggle="collapse">
          <i class="icon wb-more-horizontal" aria-hidden="true"></i>
        </button>
        <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
          <img class="navbar-brand-logo" src="<?php echo $root_dir; ?>/assets/pixl8-logo-bm.svg" title="Remark">
          <span class="navbar-brand-text hidden-xs-down"> Pixl8 Technologies</span>
        </div>
        <button type="button" class="d-none navbar-toggler collapsed" data-target="#site-navbar-search"
          data-toggle="collapse">
          <span class="sr-only">Toggle Search</span>
          <i class="icon wb-search" aria-hidden="true"></i>
        </button>
      </div>
    
      <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
          <!-- Navbar Toolbar -->
          <ul class="nav navbar-toolbar">
            <li class="nav-item hidden-float" id="toggleMenubar">
              <a class="nav-link" data-toggle="menubar" href="#" role="button">
                <i class="icon hamburger hamburger-arrow-left">
                  <span class="sr-only">Toggle menubar</span>
                  <span class="hamburger-bar"></span>
                </i>
              </a>
            </li>
            <li class="nav-item hidden-sm-down" id="toggleFullscreen">
              <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                <span class="sr-only">Toggle fullscreen</span>
              </a>
            </li>
            <li class="d-none nav-item hidden-float">
              <a class="nav-link icon wb-search" data-toggle="collapse" href="#" data-target="#site-navbar-search"
                role="button">
                <span class="sr-only">Toggle Search</span>
              </a>
            </li>
          </ul>
          <!-- End Navbar Toolbar -->
    
          <!-- Navbar Toolbar Right -->
          <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo $root_dir; ?>/" data-animation="scale-up"
                aria-expanded="false" role="button">
                <h4 class="my-0">PacoCS - Health Records Management System</h4>
              </a>
            </li>
            <li class="nav-item ">
              <a href="<?php echo $root_dir; ?>/" data-animation="scale-up"
                aria-expanded="false" role="button">
                <img class="py-10 pr-30 pl-5" style="height: 65px;" src="<?php echo $root_dir; ?>/assets/pacocs-logo.png" alt="">
              </a>
            </li>
          </ul>
          <!-- End Navbar Toolbar Right -->

        </div>
        <!-- End Navbar Collapse -->
    
        <!-- Site Navbar Seach -->
        <div class="collapse navbar-search-overlap" id="site-navbar-search">
          <form role="search" action="<?php echo $root_dir; ?>/search" method="get">
            <div class="form-group">
              <div class="input-search">
                <i class="input-search-icon wb-search" aria-hidden="true"></i>
                <input type="text" class="form-control" name="site-search" placeholder="Search..." name="q">
                <button type="submit" class="input-search-close icon wb-close" data-target="#site-navbar-search"
                  data-toggle="collapse" aria-label="Close"></button>
              </div>
            </div>
          </form>
        </div>
        <!-- End Site Navbar Seach -->
      </div>
    </nav>
    <div class="site-menubar">
      <div class="site-menubar-body">
        <div>
          <div>
            <ul class="site-menu" data-plugin="menu">
              <li class="site-menu-category">General</li>
              <li class="site-menu-item has-sub">
                <a href="<?php echo $root_dir; ?>/" aria-hidden="true">
                  <i class="site-menu-icon wb-dashboard" aria-hidden="true"></i>
                  <span class="site-menu-title">Dashboard</span>
                </a>
              </li>
              <li class="site-menu-item has-sub">
                <a href="javascript:void(0)" aria-hidden="true" id="search-to-edit-nav-btn" data-target="#search-to-edit" data-toggle="modal">
                  <i class="site-menu-icon wb-search" aria-hidden="true"></i>
                  <span class="site-menu-title">Quick Search</span>
                </a>
              </li>
              <li class="site-menu-item has-sub">
                <a href="<?php echo $root_dir; ?>/settings">
                  <i class="site-menu-icon icon wb-settings" aria-hidden="true"></i>
                  <span class="site-menu-title">Settings</span>
                </a>
              </li>
            </ul>
            <ul class="site-menu" data-plugin="menu">
              <li class="site-menu-category">Health Records</li>
              <li class="site-menu-item has-sub">
                <a href="<?php echo $root_dir; ?>/view">
                  <i class="site-menu-icon icon wb-list" aria-hidden="true"></i>
                  <span class="site-menu-title">View All</span>
                </a>
              </li>
              <li class="site-menu-item has-sub">
                <a href="<?php echo $root_dir; ?>/export" target="export-csv-iframe">
                  <i class="site-menu-icon icon wb-download" aria-hidden="true"></i>
                  <span class="site-menu-title">Export CSV</span>
                  <iframe src="" frameborder="0" name="export-csv-iframe" class="d-none"></iframe>
                </a>
              </li>
              <li class="site-menu-item has-sub">
                <a href="<?php echo $root_dir; ?>/add">
                  <i class="site-menu-icon icon wb-plus" aria-hidden="true"></i>
                  <span class="site-menu-title">Add Manually</span>
                </a>
              </li>
              <li class="site-menu-item has-sub">
                <a href="<?php echo $root_dir; ?>/import">
                  <i class="site-menu-icon icon wb-upload" aria-hidden="true"></i>
                  <span class="site-menu-title">Import CSV</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    
      <div class="site-menubar-footer">
        <!-- <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
          data-original-title="Settings">
          <span class="icon wb-settings" aria-hidden="true"></span>
        </a>
        <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
          <span class="icon wb-eye-close" aria-hidden="true"></span>
        </a> -->
        <a href="<?php echo $logDir; ?>" data-placement="top" data-toggle="tooltip" data-original-title="Logout" style="width: 100% !important;">
          <span class="icon wb-power" aria-hidden="true"></span>
        </a>
      </div>

    </div>

    <div class="site-gridmenu">
      <div>
        <div>
          <ul>
            <li>
              <a href="apps/mailbox/mailbox.html">
                <i class="icon wb-envelope"></i>
                <span>Mailbox</span>
              </a>
            </li>
            <li>
              <a href="apps/calendar/calendar.html">
                <i class="icon wb-calendar"></i>
                <span>Calendar</span>
              </a>
            </li>
            <li>
              <a href="apps/contacts/contacts.html">
                <i class="icon wb-user"></i>
                <span>Contacts</span>
              </a>
            </li>
            <li>
              <a href="apps/media/overview.html">
                <i class="icon wb-camera"></i>
                <span>Media</span>
              </a>
            </li>
            <li>
              <a href="apps/documents/categories.html">
                <i class="icon wb-order"></i>
                <span>Documents</span>
              </a>
            </li>
            <li>
              <a href="apps/projects/projects.html">
                <i class="icon wb-image"></i>
                <span>Project</span>
              </a>
            </li>
            <li>
              <a href="apps/forum/forum.html">
                <i class="icon wb-chat-group"></i>
                <span>Forum</span>
              </a>
            </li>
            <li>
              <a href="index.html">
                <i class="icon wb-dashboard"></i>
                <span>Dashboard</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="search-to-edit" aria-hidden="true" aria-labelledby="search-to-edit" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-simple">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title">Quick Search</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-btn">
                  <button id="ajax-search-by" data-value="name" type="button" class="btn btn-default btn-outline dropdown-toggle" data-toggle="dropdown"
                    aria-expanded="false">Search by Name </button>
                  <div class="dropdown-menu" role="menu" id="ajax-search-by-menus">
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem" data-value="name">Search by Name </a>
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem" data-value="student-number">Search by Student Number </a>
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem" data-value="guardian">Search by Guardian </a>
                    <a class="dropdown-item" href="javascript:void(0)" role="menuitem" data-value="class">Search by Class </a>
                  </div>
                </div>
                <button type="submit" class="input-search-btn" style="z-index: 9999;"><i class="icon wb-search" aria-hidden="true"></i></button>
                <input id="search-to-edit-input" data-home="<?php echo $root_dir; ?>" type="text" class="form-control">
              </div>
            </div>
            <div id="livesearch"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Done</button>
          </div>
        </div>
      </div>
    </div>
    <!-- End Modal -->
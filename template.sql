CREATE TABLE IF NOT EXISTS student (
    `id` integer primary key auto_increment,
    `name_last` varchar(35),
    `name_first` varchar(35),
    `name_middle` varchar(35),
    `gys` varchar(35),
    `gender` varchar(35),
    `birth_date` Date,
    `age` integer,
    `nationality` varchar(35),
    `address` varchar(100),
    `contact_no` varchar(35),
    `guardian_name` varchar(100),
    `guardian_contact_no` varchar(35),
    `guardian_address` varchar(100),
    `guardian_office_contact_no` varchar(35),
    `guardian_office_address` varchar(100),
    `alternate_person_name` varchar(100),
    `alternate_person_contact_no` varchar(35),
    `sibling_1_name` varchar(100),
    `sibling_1_cys` varchar(35),
    `sibling_2_name` varchar(100),
    `sibling_2_cys` varchar(35),
    `sibling_3_name` varchar(100),
    `sibling_3_cys` varchar(35)
);

CREATE TABLE IF NOT EXISTS medicine (
    `id` integer primary key auto_increment,
    `name` varchar(35),
    `can_be_administered` boolean,
    `student_id` integer,
    foreign key (`student_id`) REFERENCES student(`id`)
);

CREATE TABLE IF NOT EXISTS nutrition (
    `id` integer primary key auto_increment,
    `year` integer,
    `height` double,
    `weight` double,
    `bmi` double,
    `bmi_category` varchar(35),
    `student_id` integer,
    foreign key (`student_id`) REFERENCES student(`id`)
);

CREATE TABLE IF NOT EXISTS user (
    `id` integer primary key auto_increment,
    `username` varchar(35),
    `password` varchar(128)
);

INSERT INTO `user` (`id`, `username`, `password`) VALUES ('1', 'admin', 'password');

CREATE TABLE IF NOT EXISTS class (
    `id` integer primary key auto_increment,
    `name` varchar(35)
);
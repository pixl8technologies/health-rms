<?php
include("../config.php");
include("../db_connection.php");

$sql = "SELECT * FROM `student`";
$result = mysqli_query($conn, $sql);
$csv = "Last Name,First Name,Middle Name,Grade/Section,Gender,Date of Birth,Age,Nationality,Address,Contact Number,Parent or Guardian's Name,Parent or Guardian's Contact Number,Parent or Guardian's Address,Parent or Guardian's Office Contact Number,Parent or Guardian's Office Address,Altername Person Name,Alternate Person Contact Number,Sibling in School Name,Sibling in School Contact Number,Sibling in School Name,Sibling in School Contact Number,Sibling in School Name,Sibling in School Contact Number\n";

function csvqualify($value) {
    return ($value !== 'undefined' && $value !== '0' && $value !== 'null' && $value !== '') ?
    ( ( strpos( $value, '"' ) !== false ) ? str_replace( '"', '', $value ) : $value ) : '';
}

if (mysqli_num_rows($result) > 0) { while($row = mysqli_fetch_assoc($result)) {
    $csv .= "\"" . csvqualify($row['name_last']) . "\",";
    $csv .= "\"" . csvqualify($row['name_first']) . "\",";
    $csv .= "\"" . csvqualify($row['name_middle']) . "\",";
    $csv .= "\"" . csvqualify($row['gys']) . "\",";
    $csv .= "\"" . csvqualify($row['gender']) . "\",";
    $csv .= "\"" . csvqualify($row['birth_date']) . "\",";
    $csv .= "\"" . csvqualify($row['age']) . "\",";
    $csv .= "\"" . csvqualify($row['nationality']) . "\",";
    $csv .= "\"" . csvqualify($row['address']) . "\",";
    $csv .= "\"" . csvqualify($row['contact_no']) . "\",";
    $csv .= "\"" . csvqualify($row['guardian_name']) . "\",";
    $csv .= "\"" . csvqualify($row['guardian_contact_no']) . "\",";
    $csv .= "\"" . csvqualify($row['guardian_address']) . "\",";
    $csv .= "\"" . csvqualify($row['guardian_office_contact_no']) . "\",";
    $csv .= "\"" . csvqualify($row['guardian_office_address']) . "\",";
    $csv .= "\"" . csvqualify($row['alternate_person_name']) . "\",";
    $csv .= "\"" . csvqualify($row['alternate_person_contact_no']) . "\",";
    $csv .= "\"" . csvqualify($row['sibling_1_name']) . "\",";
    $csv .= "\"" . csvqualify($row['sibling_1_cys']) . "\",";
    $csv .= "\"" . csvqualify($row['sibling_2_name']) . "\",";
    $csv .= "\"" . csvqualify($row['sibling_2_cys']) . "\",";
    $csv .= "\"" . csvqualify($row['sibling_3_name']) . "\",";
    $csv .= "\"" . csvqualify($row['sibling_3_cys']) . "\"\n";
} } mysqli_close($conn);

$csv = substr( $csv, 0, ( strlen( $csv ) - 1 ) );

header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=\"Health Records_" . date("Y-m-d_h-i-s") . ".csv\"");
echo $csv;
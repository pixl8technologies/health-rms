<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page">
      <div class="page-header">
        <h1 class="page-title">Edit Student Records</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item">Student Records</li>
          <li class="breadcrumb-item active">Edit Student Records</li>
        </ol>
        <div class="page-header-actions">
          <a class="btn btn-sm btn-icon btn-default btn-outline btn-round" href="<?php echo $root_dir; ?>/import"
            data-toggle="tooltip" data-original-title="Import">
            <i class="icon wb-upload" aria-hidden="true"></i>
          </a>
          <a class="btn btn-sm btn-icon btn-default btn-outline btn-round" href="<?php echo $root_dir; ?>/add"
            data-toggle="tooltip" data-original-title="Add">
            <i class="icon wb-plus" aria-hidden="true"></i>
          </a>
          <a class="btn btn-sm btn-icon btn-default btn-outline btn-round" href="<?php echo $root_dir; ?>/export"
            data-toggle="tooltip" data-original-title="Export">
            <i class="icon wb-download" aria-hidden="true"></i></a>
        </div>
      </div>

      <div class="page-content">
        <div class="panel">
          <div class="panel-body container-fluid">
            <form autocomplete="off" action="" method="post">
              <!-- Edit Student -->
              <?php
              if (isset($_POST['edit_student_id'])) {
                $sql_error = "";
                $name_last = mysqli_real_escape_string($conn, $_POST['name_last']);
                $name_first = mysqli_real_escape_string($conn, $_POST['name_first']);
                $name_middle = mysqli_real_escape_string($conn, $_POST['name_middle']);
                $gys = mysqli_real_escape_string($conn, $_POST['gys']);
                $gender = mysqli_real_escape_string($conn, $_POST['gender']);
                $age = mysqli_real_escape_string($conn, $_POST['age']);
                $nationality = mysqli_real_escape_string($conn, $_POST['nationality']);
                $address = mysqli_real_escape_string($conn, $_POST['address']);
                $contact_no = mysqli_real_escape_string($conn, $_POST['contact_no']);
                $guardian_name = mysqli_real_escape_string($conn, $_POST['guardian_name']);
                $guardian_contact_no = mysqli_real_escape_string($conn, $_POST['guardian_contact_no']);
                $guardian_address = mysqli_real_escape_string($conn, $_POST['guardian_address']);
                $guardian_office_contact_no = mysqli_real_escape_string($conn, $_POST['guardian_office_contact_no']);
                $guardian_office_address = mysqli_real_escape_string($conn, $_POST['guardian_office_address']);
                $alternate_person_name = mysqli_real_escape_string($conn, $_POST['alternate_person_name']);
                $alternate_person_contact_no = mysqli_real_escape_string($conn, $_POST['alternate_person_contact_no']);
                $sibling_1_name = mysqli_real_escape_string($conn, $_POST['sibling_1_name']);
                $sibling_1_cys = mysqli_real_escape_string($conn, $_POST['sibling_1_cys']);
                $sibling_2_name = mysqli_real_escape_string($conn, $_POST['sibling_2_name']);
                $sibling_2_cys = mysqli_real_escape_string($conn, $_POST['sibling_2_cys']);
                $sibling_3_name = mysqli_real_escape_string($conn, $_POST['sibling_3_name']);
                $sibling_3_cys = mysqli_real_escape_string($conn, $_POST['sibling_3_cys']);
                $birth_date = strtotime($_POST["birth_date"]);
                $birth_date = date('Y-m-d H:i:s', $birth_date);
                $sql = "UPDATE student SET
                  name_last = '" . $name_last . "',
                  name_first = '" . $name_first . "',
                  name_middle = '" . $name_middle . "',
                  gys = '" . $gys . "',
                  gender = '" . $gender . "',
                  birth_date = '" . $birth_date . "',
                  age = '" . $age . "',
                  nationality = '" . $nationality . "',
                  address = '" . $address . "',
                  contact_no = '" . $contact_no . "',
                  guardian_name = '" . $guardian_name . "',
                  guardian_contact_no = '" . $guardian_contact_no . "',
                  guardian_address = '" . $guardian_address . "',
                  guardian_office_contact_no = '" . $guardian_office_contact_no . "',
                  guardian_office_address = '" . $guardian_office_address . "',
                  alternate_person_name = '" . $alternate_person_name . "',
                  alternate_person_contact_no = '" . $alternate_person_contact_no . "',
                  sibling_1_name = '" . $sibling_1_name . "',
                  sibling_1_cys = '" . $sibling_1_cys . "',
                  sibling_2_name = '" . $sibling_2_name . "',
                  sibling_2_cys = '" . $sibling_2_cys . "',
                  sibling_3_name = '" . $sibling_3_name . "',
                  sibling_3_cys = '" . $sibling_3_cys . "' WHERE id=" . $_POST["edit_student_id"];
                if ( !(mysqli_query($conn, $sql)) ) $sql_error .=  '<br>' . mysqli_error($conn);
                
                // Delete medicine data for the student
                $sql = "DELETE FROM medicine WHERE student_id=" . $_POST["edit_student_id"];
                if ( !(mysqli_query($conn, $sql)) ) $sql_error .=  '<br>' . mysqli_error($conn);
                // Create a query to add medicine data
                $sql = "INSERT INTO medicine( name, can_be_administered, student_id ) VALUES ";
                foreach ($_POST as $key => $value) { 
                  if (strpos($key, 'labelinputMedicine') === 0) {
                    $sql .= "('" . $value . "', " . (isset($_POST[str_replace('label', '', $key)]) ? true : 0) . ", " . $_POST["edit_student_id"] . "), ";
                  }
                }
                $sql = substr( $sql, 0, (strlen($sql) - 2) );
                // Insert medicine data to database
                if ( !(mysqli_query($conn, $sql)) ) $sql_error .=  '<br>' . mysqli_error($conn);

                // Delete nutrition data for the student
                $sql = "DELETE FROM nutrition WHERE student_id=" . $_POST["edit_student_id"];
                if ( !(mysqli_query($conn, $sql)) ) $sql_error .=  '<br>' . mysqli_error($conn);
                // Create a query to add nutrition data
                $hasnutritionrecord = false;
                $sql = "INSERT INTO nutrition( year, bmi, bmi_category, weight, height, student_id ) VALUES ";
                foreach ($_POST as $key => $value) { 
                  if ( strpos($key, 'nutrition_') === 0 ) {
                    $hasnutritionrecord = true;
                    if ( strpos($key, 'year') !== false ) $sql .= "(" . ( ($value == '') ? 0 : $value ) . ", ";
                    else if ( strpos($key, 'bmi') !== false ) $sql .= ( ($value == '') ? 0 : $value ) . ", ";
                    else if ( strpos($key, 'category') !== false ) $sql .= "'" . ( ($value == '') ? '' : $value ) . "', ";
                    else if ( strpos($key, 'weight') !== false ) $sql .= ( ($value == '') ? 0 : $value ) . ", ";
                    else if ( strpos($key, 'height') !== false ) $sql .= ( ($value == '') ? 0 : $value ) . ", " . $_POST["edit_student_id"] . "), ";
                  }
                }
                $sql = substr( $sql, 0, (strlen($sql) - 2) );
                // Insert nutrition data to database
                if ($hasnutritionrecord == true)
                  if ( !(mysqli_query($conn, $sql)) ) $sql_error .=  '<br>' . mysqli_error($conn);


                if ( !($sql_error === '') ) { ?>
                <div class="alert alert-warning alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  ERROR : <?php echo $sql_error; ?>
                </div>
                <?php } else { ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  SUCCESS : Student data has been updated.
                </div>
              <?php } } ?>
              <!-- End Edit Student -->
              <?php
              $sql = "SELECT * FROM student WHERE id=" . $_GET['id'];
              $result = mysqli_query($conn, $sql);
              if (mysqli_num_rows($result) > 0) { if($row = mysqli_fetch_assoc($result)) { ?>
              <input class="d-none" name="edit_student_id" value="<?= $row['id'] ?>" />
              <div class="row row-lg">
                <div class="col-md-8">
                  <!-- Example Basic Form (Form grid) -->
                  <div class="example-wrap mb-50">
                    <h4 class="example-title">Basic Information</h4>
                    <div class="example">
                      <div class="row">
                        <div class="form-group col-md-4">
                          <label class="form-control-label" for="inputFirstName">First Name</label>
                          <input type="text" class="form-control" id="inputFirstName" name="name_first"
                            placeholder="First Name" autocomplete="off" value="<?= $row['name_first'] ?>" />
                        </div>
                        <div class="form-group col-md-4">
                          <label class="form-control-label" for="inputMiddleName">Middle Name</label>
                          <input type="text" class="form-control" id="inputMiddleName" name="name_middle"
                            placeholder="Middle Name" autocomplete="off" value="<?= $row['name_middle'] ?>" />
                        </div>
                        <div class="form-group col-md-4">
                          <label class="form-control-label" for="inputLastName">Last Name</label>
                          <input type="text" class="form-control" id="inputLastName" name="name_last"
                            placeholder="Last Name" autocomplete="off" value="<?= $row['name_last'] ?>" />
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-md-3">
                          <label class="form-control-label" for="inputGYS">Grade/Year/Section</label>
                          <input type="text" class="form-control" id="inputGYS" name="gys"
                            placeholder="Grade/Year/Section" autocomplete="off" value="<?= $row['gys'] ?>" />
                        </div>
                        <div class="form-group col-md-4">
                          <label class="form-control-label">Gender</label>
                          <div>
                            <div class="radio-custom radio-default radio-inline">
                              <input type="radio" id="inputMale" name="gender" value="M" <?php echo (strcasecmp($row['gender'],'male') == 0 || strcasecmp($row['gender'],'m') == 0) ? 'checked' : ''; ?> />
                              <label for="inputMale">Male</label>
                            </div>
                            <div class="radio-custom radio-default radio-inline">
                              <input type="radio" id="inputFemale" name="gender" value="F" <?php echo (strcasecmp($row['gender'],'female') == 0 || strcasecmp($row['gender'],'f') == 0) ? 'checked' : ''; ?> />
                              <label for="inputFemale">Female</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group col-md-5">
                          <label class="form-control-label" for="inputDateOfBirth">Date of Birth</label>
                          <div class="input-group">
                            <span class="input-group-addon">
                              <i class="icon wb-calendar" aria-hidden="true"></i>
                            </span>
                            <input type="text" id="inputDateOfBirth" class="form-control" data-plugin="datepicker" name="birth_date" value="<?= date('m/d/Y', strtotime($row['birth_date'])) ?>" >
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-md-3">
                          <label class="form-control-label" for="inputAge">Age</label>
                          <input type="number" class="form-control" id="inputAge" name="age"
                            placeholder="Age" autocomplete="off" value="<?= $row['age'] ?>" />
                        </div>
                        <div class="form-group col-md-5">
                          <label class="form-control-label" for="inputNationality">Nationality</label>
                          <input type="text" class="form-control" id="inputNationality" name="nationality"
                            placeholder="Nationality" autocomplete="off" value="<?= $row['nationality'] ?>" />
                        </div>
                        <div class="form-group col-md-4">
                          <label class="form-control-label" for="inputContactNumber">Contact Number</label>
                          <input type="text" class="form-control" id="inputContactNumber" name="contact_no"
                            placeholder="Contact Number" autocomplete="off" value="<?= $row['contact_no'] ?>" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="form-control-label" for="inputAddress">Address</label>
                        <input type="text" class="form-control" id="inputAddress" name="address"
                          placeholder="Address" autocomplete="off" value="<?= $row['address'] ?>" />
                      </div>
                    </div>
                  </div>
                  <div class="example-wrap mb-50">
                    <h4 class="example-title">Parent or Guardian</h4>
                    <div class="example">
                      <div class="row">
                        <div class="form-group col-md-8">
                          <label class="form-control-label" for="inputGuardianFullName">Full Name</label>
                          <input type="text" class="form-control" id="inputGuardianFullName" name="guardian_name"
                            placeholder="Full Name" autocomplete="off" value="<?= $row['guardian_name'] ?>" />
                        </div>
                        <div class="form-group col-md-4">
                          <label class="form-control-label" for="inputGuardianContactNumber">Contact Number</label>
                          <input type="text" class="form-control" id="inputGuardianContactNumber" name="guardian_contact_no"
                            placeholder="Contact Number" autocomplete="off" value="<?= $row['guardian_contact_no'] ?>" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="form-control-label" for="inputGuardianAddress">Address</label>
                        <input type="text" class="form-control" id="inputGuardianAddress" name="guardian_address"
                          placeholder="Address" autocomplete="off" value="<?= $row['guardian_address'] ?>" />
                      </div>
                      <div class="row">
                        <div class="form-group col-md-4">
                          <label class="form-control-label" for="inputGuardianOfficeContactNumber">Office Contact Number</label>
                          <input type="text" class="form-control" id="inputGuardianOfficeContactNumber" name="guardian_office_contact_no"
                            placeholder="Contact Number" autocomplete="off" value="<?= $row['guardian_office_contact_no'] ?>" />
                        </div>
                        <div class="form-group col-md-8">
                          <label class="form-control-label" for="inputGuardianOfficeAddress">Office Address</label>
                          <input type="text" class="form-control" id="inputGuardianOfficeAddress" name="guardian_office_address"
                            placeholder="Address" autocomplete="off" value="<?= $row['guardian_office_address'] ?>" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="example-wrap mb-50">
                    <h4 class="example-title">Alternate person to be notified in case of emergency</h4>
                    <div class="example">
                      <div class="row">
                        <div class="form-group col-md-8">
                          <label class="form-control-label" for="inputAlternateFullName">Full Name</label>
                          <input type="text" class="form-control" id="inputAlternateFullName" name="alternate_person_name"
                            placeholder="Full Name" autocomplete="off" value="<?= $row['alternate_person_name'] ?>" />
                        </div>
                        <div class="form-group col-md-4">
                          <label class="form-control-label" for="inputAlternateContactNumber">Contact Number</label>
                          <input type="text" class="form-control" id="inputAlternateContactNumber" name="alternate_person_contact_no"
                            placeholder="Contact Number" autocomplete="off" value="<?= $row['alternate_person_contact_no'] ?>" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="example-wrap mb-50">
                    <h4 class="example-title">Elder siblings in school</h4>
                    <div class="example">
                      <div class="row">
                        <div class="form-group col-md-7">
                          <label class="form-control-label" for="inputSibling1FullName">Full Name</label>
                          <input type="text" class="form-control" id="inputSibling1FullName" name="sibling_1_name"
                            placeholder="Full Name" autocomplete="off" value="<?= $row['sibling_1_name'] ?>" />
                        </div>
                        <div class="form-group col-md-5">
                          <label class="form-control-label" for="inputSibling1GYS">Grade/Year/Section</label>
                          <input type="text" class="form-control" id="inputSibling1GYS" name="sibling_1_cys"
                            placeholder="Grade/Year/Section" autocomplete="off" value="<?= $row['sibling_1_cys'] ?>" />
                        </div>
                      </div>
                      <div class="row <?php echo ($row['sibling_2_name'] === "") ? 'd-none' : '' ?>" id="sibling_2">
                        <div class="form-group col-md-7">
                          <label class="form-control-label" for="inputSibling2FullName">Full Name</label>
                          <input type="text" class="form-control" id="inputSibling2FullName" name="sibling_2_name"
                            placeholder="Full Name" autocomplete="off" value="<?= $row['sibling_2_name'] ?>" />
                        </div>
                        <div class="form-group col-md-5">
                          <label class="form-control-label" for="inputSibling2GYS">Grade/Year/Section</label>
                          <input type="text" class="form-control" id="inputSibling2GYS" name="sibling_2_cys"
                            placeholder="Grade/Year/Section" autocomplete="off" value="<?= $row['sibling_2_cys'] ?>" />
                        </div>
                      </div>
                      <div class="row <?php echo ($row['sibling_3_name'] === "") ? 'd-none' : '' ?>" id="sibling_3">
                        <div class="form-group col-md-7">
                          <label class="form-control-label" for="inputSibling1FullName">Full Name</label>
                          <input type="text" class="form-control" id="inputSibling1FullName" name="sibling_3_name"
                            placeholder="Full Name" autocomplete="off" value="<?= $row['sibling_3_name'] ?>" />
                        </div>
                        <div class="form-group col-md-5">
                          <label class="form-control-label" for="inputSibling3GYS">Grade/Year/Section</label>
                          <input type="text" class="form-control" id="inputSibling3GYS" name="sibling_3_cys"
                            placeholder="Grade/Year/Section" autocomplete="off" value="<?= $row['sibling_3_cys'] ?>" />
                        </div>
                      </div>
                      <div class="text-right">
                        <button class="btn btn-outline btn-default" type="button" id="showSiblingFields">Another Sibling</button>
                      </div>
                    </div>
                  </div>
                  <!-- End Example Basic Form (Form grid) -->
                </div>
                <div class="col-md-4">
                  <!-- Example Basic Form (Form grid) -->
                  <div class="example-wrap">
                    <h4 class="example-title">Health Records</h4>
                    <div class="example">
                      <div class="form-group" id="medicineChecklist">
                        <label class="form-control-label">Medicines that can be administered as per the student's parent or guardian</label>
                        <?php $result_medicine = mysqli_query($conn, "SELECT * FROM medicine WHERE student_id=" . $_GET['id']);
                          if (mysqli_num_rows($result_medicine) > 0) { while($row_medicine = mysqli_fetch_assoc($result_medicine)) {
                            $name = $row_medicine['name'];
                            $medicineChecked[ $name ] = ($row_medicine['can_be_administered'] === "1" ? 'checked' : ''); } } ?>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineParacetamol" name="inputMedicineParacetamol"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Paracetamol']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineParacetamol">Paracetamol</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineMefenamicAcid" name="inputMedicineMefenamicAcid"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Mefenamic Acid']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineMefenamicAcid">Mefenamic Acid</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineBonamine" name="inputMedicineBonamine"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Bonamine']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineBonamine">Bonamine</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineKremilS" name="inputMedicineKremilS"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Kremil S.']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineKremilS">Kremil S.</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineVicks" name="inputMedicineVicks"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Vicks']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineVicks">Vicks</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineWhiteFlower" name="inputMedicineWhiteFlower"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['White Flower']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineWhiteFlower">White Flower</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineEfficacentOil" name="inputMedicineEfficacentOil"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Efficacent Oil']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineEfficacentOil">Efficacent Oil</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineHydrite" name="inputMedicineHydrite"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Hydrite']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineHydrite">Hydrite</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineNeozep" name="inputMedicineNeozep"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Neozep']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineNeozep">Neozep</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineCarbocistein" name="inputMedicineCarbocistein"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Carbocistein']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineCarbocistein">Carbocistein</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineSalbutanol" name="inputMedicineSalbutanol"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Salbutanol']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineSalbutanol">Salbutanol</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineLoratidine" name="inputMedicineLoratidine"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Loratidine']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineLoratidine">Loratidine</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineDesowen" name="inputMedicineDesowen"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Desowen']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineDesowen">Desowen</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineBetadine" name="inputMedicineBetadine"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Betadine']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineBetadine">Betadine</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineVisine" name="inputMedicineVisine"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Visine']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineVisine">Visine</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineBactidol" name="inputMedicineBactidol"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Bactidol']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineBactidol">Bactidol</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineBSI" name="inputMedicineBSI"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['BSI']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineBSI">BSI</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineFlammazineCream" name="inputMedicineFlammazineCream"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Flammazine Cream']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineFlammazineCream">Flammazine Cream</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineColdCompress" name="inputMedicineColdCompress"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Cold Compress']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineColdCompress">Cold Compress</label>
                        </p>
                        <p class="mb-15">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineDeguadin" name="inputMedicineDeguadin"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" <?php echo $medicineChecked['Deguadin']; ?> />
                          <label class="inputMedicineLabel" for="inputMedicineDeguadin">Deguadin</label>
                        </p>
                        <p class="d-none text-right">
                          <button class="btn btn-outline btn-default" data-target="#exampleFormModal" data-toggle="modal" type="button">Edit List</button>
                        </p>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleFormModal" aria-hidden="false" aria-labelledby="exampleFormModalLabel"
                          role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-simple">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="exampleFormModalLabel">Edit the List of Medicines</h4>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="mb-15">
                                      <p>This is not yet functional. Please close.</p>
                                      <button id="addToTable" class="btn btn-outline btn-primary" type="button">
                                        <i class="icon wb-plus" aria-hidden="true"></i> Add medicine
                                      </button>
                                    </div>
                                  </div>
                                </div>
                                <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleAddRow">
                                  <thead>
                                    <tr>
                                      <th>Rendering engine</th>
                                      <th>Actions</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr class="gradeA">
                                      <td>Trident</td>
                                      <td class="actions">
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-editing save-row"
                                          data-toggle="tooltip" data-original-title="Save" hidden><i class="icon wb-wrench" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-editing cancel-row"
                                          data-toggle="tooltip" data-original-title="Delete" hidden><i class="icon wb-close" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                                          data-toggle="tooltip" data-original-title="Edit"><i class="icon wb-edit" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                                          data-toggle="tooltip" data-original-title="Remove"><i class="icon wb-trash" aria-hidden="true"></i></a>
                                      </td>
                                    </tr>
                                    <tr class="gradeA">
                                      <td>Trident</td>
                                      <td class="actions">
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-editing save-row"
                                          data-toggle="tooltip" data-original-title="Save" hidden><i class="icon wb-wrench" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-editing cancel-row"
                                          data-toggle="tooltip" data-original-title="Delete" hidden><i class="icon wb-close" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                                          data-toggle="tooltip" data-original-title="Edit"><i class="icon wb-edit" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                                          data-toggle="tooltip" data-original-title="Remove"><i class="icon wb-trash" aria-hidden="true"></i></a>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Modal -->
                        <label class="form-control-label mt-20">Nutrition Records</label>
                        <table class="table table-hover toggle-circle" id="nutrition-accordion">
                          <thead>
                            <tr>
                              <th>Year</th>
                              <th>BMI</th>
                              <th>Category</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php $result_nutrition = mysqli_query($conn, "SELECT * FROM nutrition WHERE student_id=" . $_GET['id']);
                            if (mysqli_num_rows($result_nutrition) > 0) { while($row_nutrition = mysqli_fetch_assoc($result_nutrition)) { ?>
                            <tr class="nutrition-accordion" style="cursor: pointer;">
                              <td><?php echo $row_nutrition['year']; ?></td>
                              <td><?php echo $row_nutrition['bmi']; ?></td>
                              <td><?php echo $row_nutrition['bmi_category']; ?></td>
                              <td><i class="icon wb-chevron-right-mini"></i></td>
                            </tr>
                            <tr class="d-none">
                              <th colspan="2">Weight (kg)</th>
                              <td colspan="2"><?php echo $row_nutrition['weight']; ?></td>
                            </tr>
                            <tr class="d-none">
                              <th colspan="2">Height (cm)</th>
                              <td colspan="2"><?php echo $row_nutrition['height']; ?></td>
                            </tr>
                            <?php } } else { ?>
                            <tr class="nutrition-accordion" style="cursor: pointer;">
                              <td>2016</td>
                              <td></td>
                              <td></td>
                              <td><i class="icon wb-chevron-right-mini"></i></td>
                            </tr>
                            <tr class="d-none">
                              <th colspan="2">Weight (kg)</th>
                              <td colspan="2"></td>
                            </tr>
                            <tr class="d-none">
                              <th colspan="2">Height (cm)</th>
                              <td colspan="2"></td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                        <div class="text-right">
                          <button class="btn btn-outline btn-default" data-target="#exampleNiftySlideFromBottom" data-toggle="modal" type="button" id="editBMIList">Edit List</button>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade modal-slide-from-bottom" id="exampleNiftySlideFromBottom"
                          aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                          tabindex="-1">
                          <div class="modal-dialog modal-simple">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Edit Nutrition Records</h4>
                              </div>
                              <div class="modal-body">
                                <p>Click each cells to edit.</p>
                                <table class="editable-table table table-striped" id="editableTable">
                                  <thead>
                                    <tr>
                                      <th>Year</th>
                                      <th>Weight (kg)</th>
                                      <th>Height (cm)</th>
                                      <th>BMI</th>
                                      <th>Category</th>
                                      <th>Actions</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php $result_nutrition = mysqli_query($conn, "SELECT * FROM nutrition WHERE student_id=" . $_GET['id']);
                                    if (mysqli_num_rows($result_nutrition) > 0) { while($row_nutrition = mysqli_fetch_assoc($result_nutrition)) { ?>
                                    <tr>
                                      <td class="year"><?php echo $row_nutrition['year']; ?></td>
                                      <td class="weight"><?php echo $row_nutrition['weight']; ?></td>
                                      <td class="height"><?php echo $row_nutrition['height']; ?></td>
                                      <th class="bmi"><?php echo $row_nutrition['bmi']; ?></th>
                                      <th class="category"><?php echo $row_nutrition['bmi_category']; ?></th>
                                      <th>
                                        <span class="icon wb-trash" aria-hidden="true"></span> &nbsp;
                                        <span class="icon wb-plus-circle" aria-hidden="true"></span>
                                      </th>
                                    </tr>
                                    <?php } } else { ?>
                                    <tr>
                                      <td class="year"></td>
                                      <td class="weight"></td>
                                      <td class="height"></td>
                                      <th class="bmi"></th>
                                      <th class="category"></th>
                                      <th>
                                        <span class="icon wb-trash" aria-hidden="true"></span> &nbsp;
                                        <span class="icon wb-plus-circle" aria-hidden="true"></span>
                                      </th>
                                    </tr>
                                    <?php } ?>
                                  </tbody>
                                </table>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="nutritionSaveChanges">Save changes</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Modal -->
                      </div>
                    </div>
                  </div>
                  <!-- End Example Basic Form (Form grid) -->
                </div>
                <div class="col-md-12 text-right">
                  <div class="form-group">
                    <button type="reset" class="btn btn-default mr-10">Reset</button>
                    <button type="submit" class="btn btn-primary">Submit Data</button>
                  </div>
                </div>
              </div>
              <?php } } ?>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>
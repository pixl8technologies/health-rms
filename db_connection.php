<?php

// Create database if it doesn't exist
try { $mysqli = new mysqli($db_host, $db_user, $db_password); }
catch (\Exception $e) { echo $e->getMessage(), PHP_EOL; }
if ($mysqli->select_db($db_name) === false) {
	$conn = mysqli_connect($db_host, $db_user, $db_password);
	$sql = "CREATE DATABASE `" . $db_name . "`";
	if (!($conn->query($sql) === TRUE)) {
		echo "Error creating database: " . $conn->error;
		die();
	} else {
		$filename = $_SERVER['DOCUMENT_ROOT'] . $root_dir . '/template.sql';
		$sql = '';
		$lines = file($filename);
		foreach ($lines as $line) {
			if (substr($line, 0, 2) == '--' || $line == '') continue;
			$sql .= $line;
			if (substr(trim($line), -1, 1) == ';') {
				mysqli_select_db($conn, $db_name) or print('Error performing query \'<strong>' . $sql . '\': ' . mysqli_error($conn) . '<br /><br />');
				mysqli_query($conn, $sql) or print('Error performing query \'<strong>' . $sql . '\': ' . mysqli_error($conn) . '<br /><br />');
				$sql = '';
			}
		}
	}
}

$conn = mysqli_connect($db_host, $db_user, $db_password, $db_name);
if (mysqli_connect_errno()){
	echo "Failed to connect" . mysqli_connect_error();
}
?>
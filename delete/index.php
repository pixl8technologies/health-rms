<?php
include("../config.php");
include("../db_connection.php");

if (!isset($_GET['id'])) {
    die();
}
else {
    $sql = "DELETE FROM `medicine` WHERE student_id=" . $_GET['id'];
    $sql_success = true;
    $sql_error = "";
    if ( !(mysqli_query($conn, $sql)) ) {
        $sql_success = false;
        $sql_error = mysqli_error($conn) . ". ";
    }
    // echo '<pre>Delete medicines: <br>$sql_success: ';
    // var_dump($sql_success);
    // echo '<br>$sql_error: ';
    // var_dump($sql_error);
    // echo '</pre>';

    $sql = "DELETE FROM `nutrition` WHERE student_id=" . $_GET['id'];
    $sql_success = true;
    $sql_error = "";
    if ( !(mysqli_query($conn, $sql)) ) {
        $sql_success = false;
        $sql_error = mysqli_error($conn) . ". ";
    }
    // echo '<pre>Delete nutrition: <br>$sql_success: ';
    // var_dump($sql_success);
    // echo '<br>$sql_error: ';
    // var_dump($sql_error);
    // echo '</pre>';

    $sql = "DELETE FROM `student` WHERE id=" . $_GET['id'];
    $sql_success = true;
    $sql_error = "";
    if ( !(mysqli_query($conn, $sql)) ) {
        $sql_success = false;
        $sql_error = mysqli_error($conn) . ". ";
    }
    // echo '<pre>Delete student: <br>$sql_success: ';
    // var_dump($sql_success);
    // echo '<br>$sql_error: ';
    // var_dump($sql_error);
    // echo '</pre>'; 
    echo ($sql_success) ? '<div>success-' . $_GET['id'] . '</div>' : '<div>error-' . $_GET['id'] . '</div>';
}
mysqli_close($conn); ?>
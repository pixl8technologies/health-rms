<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page">
      <div class="page-header">
        <h1 class="page-title">Manually Add Student Records</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item">Student Records</li>
          <li class="breadcrumb-item active">Manually Add Student Records</li>
        </ol>
        <div class="page-header-actions">
          <a class="btn btn-sm btn-icon btn-default btn-outline btn-round" href="<?php echo $root_dir; ?>/import"
            data-toggle="tooltip" data-original-title="Import">
            <i class="icon wb-upload" aria-hidden="true"></i>
          </a>
          <a class="btn btn-sm btn-icon btn-default btn-outline btn-round" href="<?php echo $root_dir; ?>/add"
            data-toggle="tooltip" data-original-title="Add">
            <i class="icon wb-plus" aria-hidden="true"></i>
          </a>
          <a class="btn btn-sm btn-icon btn-default btn-outline btn-round" href="<?php echo $root_dir; ?>/export"
            data-toggle="tooltip" data-original-title="Export">
            <i class="icon wb-download" aria-hidden="true"></i></a>
        </div>
      </div>

      <div class="page-content">
        <div class="panel">
          <div class="panel-body container-fluid">
            <form autocomplete="off" action="add.php" method="post">
              <div class="row row-lg">
                <div class="col-md-8">
                  <!-- Example Basic Form (Form grid) -->
                  <div class="example-wrap mb-50">
                    <h4 class="example-title">Basic Information</h4>
                    <div class="example">
                      <div class="row">
                        <div class="form-group col-md-4">
                          <label class="form-control-label" for="inputLastName">Last Name</label>
                          <input type="text" class="form-control" id="inputLastName" name="name_last"
                            placeholder="Last Name" autocomplete="off" />
                        </div>
                        <div class="form-group col-md-4">
                          <label class="form-control-label" for="inputFirstName">First Name</label>
                          <input type="text" class="form-control" id="inputFirstName" name="name_first"
                            placeholder="First Name" autocomplete="off" />
                        </div>
                        <div class="form-group col-md-4">
                          <label class="form-control-label" for="inputMiddleName">Middle Name</label>
                          <input type="text" class="form-control" id="inputMiddleName" name="name_middle"
                            placeholder="Middle Name" autocomplete="off" />
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-md-3">
                          <label class="form-control-label" for="inputGYS">Grade/Section</label>
                          <select id="inputGYS" name="gys" class="form-control" data-plugin="select2">
                          <?php
                          $sql = "SELECT * FROM `class`";
                          $result = mysqli_query($conn, $sql);
                          if (mysqli_num_rows($result) > 0) { while($row = mysqli_fetch_assoc($result)) { ?>
                            <option value="<?= $row['name'] ?>"><?= $row['name'] ?></option>
                          <?php } } ?>
                          </select>
                        </div>
                        <div class="form-group col-md-4">
                          <label class="form-control-label">Gender</label>
                          <div>
                            <div class="radio-custom radio-default radio-inline">
                              <input type="radio" id="inputMale" name="gender" value="M" checked />
                              <label for="inputMale">Male</label>
                            </div>
                            <div class="radio-custom radio-default radio-inline">
                              <input type="radio" id="inputFemale" name="gender" value="F" />
                              <label for="inputFemale">Female</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group col-md-5">
                          <label class="form-control-label" for="inputDateOfBirth">Date of Birth</label>
                          <div class="input-group">
                            <span class="input-group-addon">
                              <i class="icon wb-calendar" aria-hidden="true"></i>
                            </span>
                            <input type="text" id="inputDateOfBirth" class="form-control" data-plugin="datepicker" name="birth_date">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="form-group col-md-3">
                          <label class="form-control-label" for="inputAge">Age</label>
                          <input type="number" class="form-control" id="inputAge" name="age"
                            placeholder="Age" autocomplete="off" />
                        </div>
                        <div class="form-group col-md-5">
                          <label class="form-control-label" for="inputNationality">Nationality</label>
                          <input type="text" class="form-control" id="inputNationality" name="nationality"
                            placeholder="Nationality" autocomplete="off" />
                        </div>
                        <div class="form-group col-md-4">
                          <label class="form-control-label" for="inputContactNumber">Contact Number</label>
                          <input type="text" class="form-control" id="inputContactNumber" name="contact_no"
                            placeholder="Contact Number" autocomplete="off" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="form-control-label" for="inputAddress">Address</label>
                        <input type="text" class="form-control" id="inputAddress" name="address"
                          placeholder="Address" autocomplete="off" />
                      </div>
                    </div>
                  </div>
                  <div class="example-wrap mb-50">
                    <h4 class="example-title">Parent or Guardian</h4>
                    <div class="example">
                      <div class="row">
                        <div class="form-group col-md-8">
                          <label class="form-control-label" for="inputGuardianFullName">Full Name</label>
                          <input type="text" class="form-control" id="inputGuardianFullName" name="guardian_name"
                            placeholder="Full Name" autocomplete="off" />
                        </div>
                        <div class="form-group col-md-4">
                          <label class="form-control-label" for="inputGuardianContactNumber">Contact Number</label>
                          <input type="text" class="form-control" id="inputGuardianContactNumber" name="guardian_contact_no"
                            placeholder="Contact Number" autocomplete="off" />
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="form-control-label" for="inputGuardianAddress">Address</label>
                        <input type="text" class="form-control" id="inputGuardianAddress" name="guardian_address"
                          placeholder="Address" autocomplete="off" />
                      </div>
                      <div class="row">
                        <div class="form-group col-md-4">
                          <label class="form-control-label" for="inputGuardianOfficeContactNumber">Office Contact Number</label>
                          <input type="text" class="form-control" id="inputGuardianOfficeContactNumber" name="guardian_office_contact_no"
                            placeholder="Contact Number" autocomplete="off" />
                        </div>
                        <div class="form-group col-md-8">
                          <label class="form-control-label" for="inputGuardianOfficeAddress">Office Address</label>
                          <input type="text" class="form-control" id="inputGuardianOfficeAddress" name="guardian_office_address"
                            placeholder="Address" autocomplete="off" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="example-wrap mb-50">
                    <h4 class="example-title">Alternate person to be notified in case of emergency</h4>
                    <div class="example">
                      <div class="row">
                        <div class="form-group col-md-8">
                          <label class="form-control-label" for="inputAlternateFullName">Full Name</label>
                          <input type="text" class="form-control" id="inputAlternateFullName" name="alternate_person_name"
                            placeholder="Full Name" autocomplete="off" />
                        </div>
                        <div class="form-group col-md-4">
                          <label class="form-control-label" for="inputAlternateContactNumber">Contact Number</label>
                          <input type="text" class="form-control" id="inputAlternateContactNumber" name="alternate_person_contact_no"
                            placeholder="Contact Number" autocomplete="off" />
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="example-wrap mb-50">
                    <h4 class="example-title">Elder siblings in school</h4>
                    <div class="example">
                      <div class="row">
                        <div class="form-group col-md-7">
                          <label class="form-control-label" for="inputSibling1FullName">Full Name</label>
                          <input type="text" class="form-control" id="inputSibling1FullName" name="sibling_1_name"
                            placeholder="Full Name" autocomplete="off" />
                        </div>
                        <div class="form-group col-md-5">
                          <label class="form-control-label" for="inputSibling1GYS">Grade/Section</label>
                          <select id="inputSibling1GYS" name="sibling_1_cys" class="form-control" data-plugin="select2">
                          <?php
                          $sql = "SELECT * FROM `class`";
                          $result = mysqli_query($conn, $sql);
                          if (mysqli_num_rows($result) > 0) { while($row = mysqli_fetch_assoc($result)) { ?>
                            <option value="<?= $row['name'] ?>"><?= $row['name'] ?></option>
                          <?php } } ?>
                          </select>
                        </div>
                      </div>
                      <div class="row d-none" id="sibling_2">
                        <div class="form-group col-md-7">
                          <label class="form-control-label" for="inputSibling2FullName">Full Name</label>
                          <input type="text" class="form-control" id="inputSibling2FullName" name="sibling_2_name"
                            placeholder="Full Name" autocomplete="off" />
                        </div>
                        <div class="form-group col-md-5">
                          <label class="form-control-label" for="inputSibling2GYS">Grade/Section</label>
                          <select id="inputSibling2GYS" name="sibling_2_cys" class="form-control" data-plugin="select2">
                          <?php
                          $sql = "SELECT * FROM `class`";
                          $result = mysqli_query($conn, $sql);
                          if (mysqli_num_rows($result) > 0) { while($row = mysqli_fetch_assoc($result)) { ?>
                            <option value="<?= $row['name'] ?>"><?= $row['name'] ?></option>
                          <?php } } ?>
                          </select>
                        </div>
                      </div>
                      <div class="row d-none" id="sibling_3">
                        <div class="form-group col-md-7">
                          <label class="form-control-label" for="inputSibling1FullName">Full Name</label>
                          <input type="text" class="form-control" id="inputSibling1FullName" name="sibling_3_name"
                            placeholder="Full Name" autocomplete="off" />
                        </div>
                        <div class="form-group col-md-5">
                          <label class="form-control-label" for="inputSibling3GYS">Grade/Section</label>
                          <select id="inputSibling3GYS" name="sibling_3_cys" class="form-control" data-plugin="select2">
                          <?php
                          $sql = "SELECT * FROM `class`";
                          $result = mysqli_query($conn, $sql);
                          if (mysqli_num_rows($result) > 0) { while($row = mysqli_fetch_assoc($result)) { ?>
                            <option value="<?= $row['name'] ?>"><?= $row['name'] ?></option>
                          <?php } } ?>
                          </select>
                        </div>
                      </div>
                      <div class="text-right">
                        <button class="btn btn-outline btn-default" type="button" id="showSiblingFields">Another Sibling</button>
                      </div>
                    </div>
                  </div>
                  <!-- End Example Basic Form (Form grid) -->
                </div>
                <div class="col-md-4">
                  <!-- Example Basic Form (Form grid) -->
                  <div class="example-wrap">
                    <h4 class="example-title">Health Records</h4>
                    <div class="example">
                      <div class="form-group" id="medicineChecklist">
                        <label class="form-control-label">Medicines that can be administered as per the student's parent or guardian</label>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineParacetamol" name="inputMedicineParacetamol"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineParacetamol">Paracetamol</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineMefenamicAcid" name="inputMedicineMefenamicAcid"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineMefenamicAcid">Mefenamic Acid</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineBonamine" name="inputMedicineBonamine"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineBonamine">Bonamine</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineKremilS" name="inputMedicineKremilS"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineKremilS">Kremil S.</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineVicks" name="inputMedicineVicks"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineVicks">Vicks</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineWhiteFlower" name="inputMedicineWhiteFlower"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineWhiteFlower">White Flower</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineEfficacentOil" name="inputMedicineEfficacentOil"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineEfficacentOil">Efficacent Oil</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineHydrite" name="inputMedicineHydrite"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineHydrite">Hydrite</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineNeozep" name="inputMedicineNeozep"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineNeozep">Neozep</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineCarbocistein" name="inputMedicineCarbocistein"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineCarbocistein">Carbocistein</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineSalbutanol" name="inputMedicineSalbutanol"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineSalbutanol">Salbutanol</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineLoratidine" name="inputMedicineLoratidine"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineLoratidine">Loratidine</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineDesowen" name="inputMedicineDesowen"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineDesowen">Desowen</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineBetadine" name="inputMedicineBetadine"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineBetadine">Betadine</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineVisine" name="inputMedicineVisine"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineVisine">Visine</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineBactidol" name="inputMedicineBactidol"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineBactidol">Bactidol</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineBSI" name="inputMedicineBSI"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineBSI">BSI</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineFlammazineCream" name="inputMedicineFlammazineCream"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineFlammazineCream">Flammazine Cream</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineColdCompress" name="inputMedicineColdCompress"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineColdCompress">Cold Compress</label>
                        </p>
                        <p class="mb-15 medicine-item">
                          <input type="checkbox" class="icheckbox-primary" id="inputMedicineDeguadin" name="inputMedicineDeguadin"
                            data-plugin="iCheck" data-checkbox-class="icheckbox_flat-blue" checked />
                          <label class="inputMedicineLabel" for="inputMedicineDeguadin">Deguadin</label>
                        </p>
                        <p class="d-none text-right">
                          <button class="btn btn-outline btn-default" data-target="#exampleFormModal" data-toggle="modal" type="button">Edit List</button>
                        </p>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleFormModal" aria-hidden="false" aria-labelledby="exampleFormModalLabel"
                          role="dialog" tabindex="-1">
                          <div class="modal-dialog modal-simple">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="exampleFormModalLabel">Edit the List of Medicines</h4>
                              </div>
                              <div class="modal-body">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="mb-15">
                                      <p>This is not yet functional. Please close.</p>
                                      <button id="addToTable" class="btn btn-outline btn-primary" type="button">
                                        <i class="icon wb-plus" aria-hidden="true"></i> Add medicine
                                      </button>
                                    </div>
                                  </div>
                                </div>
                                <table class="table table-bordered table-hover table-striped" cellspacing="0" id="exampleAddRow">
                                  <thead>
                                    <tr>
                                      <th>Rendering engine</th>
                                      <th>Actions</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr class="gradeA">
                                      <td>Trident</td>
                                      <td class="actions">
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-editing save-row"
                                          data-toggle="tooltip" data-original-title="Save" hidden><i class="icon wb-wrench" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-editing cancel-row"
                                          data-toggle="tooltip" data-original-title="Delete" hidden><i class="icon wb-close" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                                          data-toggle="tooltip" data-original-title="Edit"><i class="icon wb-edit" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                                          data-toggle="tooltip" data-original-title="Remove"><i class="icon wb-trash" aria-hidden="true"></i></a>
                                      </td>
                                    </tr>
                                    <tr class="gradeA">
                                      <td>Trident</td>
                                      <td class="actions">
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-editing save-row"
                                          data-toggle="tooltip" data-original-title="Save" hidden><i class="icon wb-wrench" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-editing cancel-row"
                                          data-toggle="tooltip" data-original-title="Delete" hidden><i class="icon wb-close" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default edit-row"
                                          data-toggle="tooltip" data-original-title="Edit"><i class="icon wb-edit" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-sm btn-icon btn-pure btn-default on-default remove-row"
                                          data-toggle="tooltip" data-original-title="Remove"><i class="icon wb-trash" aria-hidden="true"></i></a>
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Modal -->
                        <label class="form-control-label mt-20">Nutrition Records</label>
                        <table class="table table-hover toggle-circle" id="nutrition-accordion">
                          <thead>
                            <tr>
                              <th>Year</th>
                              <th>BMI</th>
                              <th>Category</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="nutrition-accordion">
                              <td class="text-center" colspan="9999">No records</td>
                            </tr>
                          </tbody>
                        </table>
                        <div class="text-right">
                          <button class="btn btn-outline btn-default" data-target="#exampleNiftySlideFromBottom" data-toggle="modal" type="button" id="editBMIList">Edit List</button>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade modal-slide-from-bottom" id="exampleNiftySlideFromBottom"
                          aria-hidden="true" aria-labelledby="exampleModalTitle" role="dialog"
                          tabindex="-1">
                          <div class="modal-dialog modal-simple">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title">Edit Nutrition Records</h4>
                              </div>
                              <div class="modal-body">
                                <p>Click each cells to edit.</p>
                                <table class="editable-table table table-striped" id="editableTable">
                                  <thead>
                                    <tr>
                                      <th>Year</th>
                                      <th>Weight (kg)</th>
                                      <th>Height (cm)</th>
                                      <th>BMI</th>
                                      <th>Category</th>
                                      <th>Actions</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td class="year"></td>
                                      <td class="weight"></td>
                                      <td class="height"></td>
                                      <th class="bmi"></th>
                                      <th class="category"></th>
                                      <th>
                                        <span class="icon wb-trash" aria-hidden="true"></span> &nbsp;
                                        <span class="icon wb-plus-circle" aria-hidden="true"></span>
                                      </th>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="nutritionSaveChanges">Save changes</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Modal -->
                      </div>
                    </div>
                  </div>
                  <!-- End Example Basic Form (Form grid) -->
                </div>
                <div class="col-md-12 text-right">
                  <div class="form-group">
                    <button type="reset" class="btn btn-default mr-10">Reset</button>
                    <button type="submit" class="btn btn-primary">Submit Data</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>
<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page">
      <div class="page-header">
        <h1 class="page-title">Manually Add Student Records</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item">Student Records</li>
          <li class="breadcrumb-item active">Manually Add Student Records</li>
        </ol>
        <div class="page-header-actions">
          <button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
            data-toggle="tooltip" data-original-title="Edit">
            <i class="icon wb-pencil" aria-hidden="true"></i>
          </button>
          <button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
            data-toggle="tooltip" data-original-title="Refresh">
            <i class="icon wb-refresh" aria-hidden="true"></i>
          </button>
          <button type="button" class="btn btn-sm btn-icon btn-default btn-outline btn-round"
            data-toggle="tooltip" data-original-title="Setting">
            <i class="icon wb-settings" aria-hidden="true"></i>
          </button>
        </div>
      </div>

      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
          <div class="col-xxl-7 col-lg-7">
            <!-- Widget Linearea Color -->
            <div class="card card-shadow card-responsive" id="widgetLineareaColor">
              <div class="card-block p-0">
                <div class="pt-30 p-30" style="height:calc(100% - 250px);">
                  <div class="row">
                    <div class="col-12">
                      <?php
                      $sql_error = "";
                      $name_last = mysqli_real_escape_string($conn, $_POST['name_last']);
                      $name_first = mysqli_real_escape_string($conn, $_POST['name_first']);
                      $name_middle = mysqli_real_escape_string($conn, $_POST['name_middle']);
                      $gys = mysqli_real_escape_string($conn, $_POST['gys']);
                      $gender = mysqli_real_escape_string($conn, $_POST['gender']);
                      $age = mysqli_real_escape_string($conn, $_POST['age']);
                      $nationality = mysqli_real_escape_string($conn, $_POST['nationality']);
                      $address = mysqli_real_escape_string($conn, $_POST['address']);
                      $contact_no = mysqli_real_escape_string($conn, $_POST['contact_no']);
                      $guardian_name = mysqli_real_escape_string($conn, $_POST['guardian_name']);
                      $guardian_contact_no = mysqli_real_escape_string($conn, $_POST['guardian_contact_no']);
                      $guardian_address = mysqli_real_escape_string($conn, $_POST['guardian_address']);
                      $guardian_office_contact_no = mysqli_real_escape_string($conn, $_POST['guardian_office_contact_no']);
                      $guardian_office_address = mysqli_real_escape_string($conn, $_POST['guardian_office_address']);
                      $alternate_person_name = mysqli_real_escape_string($conn, $_POST['alternate_person_name']);
                      $alternate_person_contact_no = mysqli_real_escape_string($conn, $_POST['alternate_person_contact_no']);
                      $sibling_1_name = mysqli_real_escape_string($conn, $_POST['sibling_1_name']);
                      $sibling_1_cys = mysqli_real_escape_string($conn, $_POST['sibling_1_cys']);
                      $sibling_2_name = mysqli_real_escape_string($conn, $_POST['sibling_2_name']);
                      $sibling_2_cys = mysqli_real_escape_string($conn, $_POST['sibling_2_cys']);
                      $sibling_3_name = mysqli_real_escape_string($conn, $_POST['sibling_3_name']);
                      $sibling_3_cys = mysqli_real_escape_string($conn, $_POST['sibling_3_cys']);
                      $birth_date = strtotime($_POST["birth_date"]);
                      $birth_date = date('Y-m-d H:i:s', $birth_date);
                      $sql = "INSERT INTO `student`(
                        `name_last`,
                        `name_first`,
                        `name_middle`,
                        `gys`,
                        `gender`,
                        `birth_date`,
                        `age`,
                        `nationality`,
                        `address`,
                        `contact_no`,
                        `guardian_name`,
                        `guardian_contact_no`,
                        `guardian_address`,
                        `guardian_office_contact_no`,
                        `guardian_office_address`,
                        `alternate_person_name`,
                        `alternate_person_contact_no`,
                        `sibling_1_name`,
                        `sibling_1_cys`,
                        `sibling_2_name`,
                        `sibling_2_cys`,
                        `sibling_3_name`,
                        `sibling_3_cys`) VALUES (
                        '" . $name_last . "',
                        '" . $name_first . "',
                        '" . $name_middle . "',
                        '" . $gys . "',
                        '" . $gender . "',
                        '" . $birth_date . "',
                        '" . $age . "',
                        '" . $nationality . "',
                        '" . $address . "',
                        '" . $contact_no . "',
                        '" . $guardian_name . "',
                        '" . $guardian_contact_no . "',
                        '" . $guardian_address . "',
                        '" . $guardian_office_contact_no . "',
                        '" . $guardian_office_address . "',
                        '" . $alternate_person_name . "',
                        '" . $alternate_person_contact_no . "',
                        '" . $sibling_1_name . "',
                        '" . $sibling_1_cys . "',
                        '" . $sibling_2_name . "',
                        '" . $sibling_2_cys . "',
                        '" . $sibling_3_name . "',
                        '" . $sibling_3_cys . "'
                      )";

                      $sql_success = true;
                      if ( !(mysqli_query($conn, $sql)) ) {
                        $sql_success = false;
                        $sql_error = mysqli_error($conn) . ". ";
                      }
                      $student_id = mysqli_insert_id($conn);

                      // Create a query to add medicine data
                      $sql = "INSERT INTO `medicine`( `name`, `can_be_administered`, `student_id` ) VALUES ";
                      foreach ($_POST as $key => $value) { 
                        if (strpos($key, 'labelinputMedicine') === 0) {
                          $sql .= "('" . $value . "', " . (isset($_POST[str_replace('label', '', $key)]) ? true : 0) . ", " . $student_id . "), ";
                        }
                      }
                      $sql = substr( $sql, 0, (strlen($sql) - 2) );
                      // Insert medicine data to database
                      if ( !(mysqli_query($conn, $sql)) ) {
                        $sql_success = false;
                        $sql_error .= mysqli_error($conn) . ". ";
                      }

                      // Create a query to add nutrition data
                      $hasnutritionrecord = false;
                      $sql = "INSERT INTO `nutrition`( `year`, `bmi`, `bmi_category`, `weight`, `height`, `student_id` ) VALUES ";
                      foreach ($_POST as $key => $value) { 
                        if ( strpos($key, 'nutrition_') === 0 ) {
                          $hasnutritionrecord = true;
                          if ( strpos($key, 'year') !== false ) $sql .= "(" . ( ($value == '') ? 0 : $value ) . ", ";
                          else if ( strpos($key, 'bmi') !== false ) $sql .= ( ($value == '') ? 0 : $value ) . ", ";
                          else if ( strpos($key, 'category') !== false ) $sql .= "'" . ( ($value == '') ? '' : $value ) . "', ";
                          else if ( strpos($key, 'weight') !== false ) $sql .= ( ($value == '') ? 0 : $value ) . ", ";
                          else if ( strpos($key, 'height') !== false ) $sql .= ( ($value == '') ? 0 : $value ) . ", " . $student_id . "), ";
                        }
                      }
                      $sql = substr( $sql, 0, (strlen($sql) - 2) );
                      // Insert nutrition data to database
                      if ($hasnutritionrecord == true) {
                        if ( !(mysqli_query($conn, $sql)) ) {
                          $sql_success = false;
                          $sql_error .= mysqli_error($conn) . ". ";
                        }
                      }

                      mysqli_close($conn);
                      
                      if ($sql_success) { ?>
                      <h2 class="mt-0">Successfully added.</h2>
                      <p class="mb-25">Your request has been processed.</p>
                      <a href="<?php echo $root_dir; ?>/add" class="btn btn-primary btn-md mr-5" >Add More</a>
                      <a href="<?php echo $root_dir; ?>/view" class="btn btn-outline btn-primary btn-md" >View All</a>
                      <?php } else { ?>
                      <h2 class="mt-0 text-danger">There was an error in processing your request.</h2>
                      <p>Please make sure you put acceptable input. <?php echo $sql_error; ?></p>
                      <a href="<?php echo $root_dir; ?>/add" class="btn btn-default btn-md" >Try to add again</a>
                      <?php } ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Widget Linearea Color -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>
<?php include("../header.php"); ?>

    <!-- Page -->
    <div class="page view-all-page">
      <div class="page-header">
        <h1 class="page-title">View All Student Health Records</h1>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo $root_dir; ?>/">Home</a></li>
          <li class="breadcrumb-item">Student Records</li>
          <li class="breadcrumb-item active">View All Student Health Records</li>
        </ol>
        <div class="page-header-actions">
          <a class="btn btn-sm btn-icon btn-default btn-outline btn-round" href="<?php echo $root_dir; ?>/import"
            data-toggle="tooltip" data-original-title="Import">
            <i class="icon wb-upload" aria-hidden="true"></i>
          </a>
          <a class="btn btn-sm btn-icon btn-default btn-outline btn-round" href="<?php echo $root_dir; ?>/add"
            data-toggle="tooltip" data-original-title="Add">
            <i class="icon wb-plus" aria-hidden="true"></i>
          </a>
          <a class="btn btn-sm btn-icon btn-default btn-outline btn-round" href="<?php echo $root_dir; ?>/export"
            data-toggle="tooltip" data-original-title="Export">
            <i class="icon wb-download" aria-hidden="true"></i></a>
        </div>
      </div>
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">
          <div class="col-lg-12">

            <!-- Panel Accordion -->
            <div class="panel">
              <div class="panel-body collapse show" id="exampleFooAccordionPanel">
                <?php
                $rows_per_page = (isset($_GET['rows'])) ? $_GET['rows'] : 10;
                $student_count = mysqli_fetch_array( mysqli_query( $conn, "SELECT COUNT(`id`) FROM student" ) )[0];
                $page_count = ceil((int)$student_count / $rows_per_page);
                if (isset($_GET['page'])) {
                  if ( $_GET['page'] <= $page_count )
                    $current_page = $_GET['page'];
                  else {
                    $current_page = $page_count;
                    $_GET['page'] = $current_page;
                  }
                }
                else
                  $current_page = 1;
                ?>
                <div class="row mb-15">
                  <div class="col-md-6">
                    <form action="<?php echo $root_dir; ?>/search" method="get" class="form-inline d-inline-block mr-10">
                      <div class="form-group footable-filtering-search">
                        <div class="input-group">
                          <input type="text" class="form-control p-18" placeholder="Search" name="q" />
                          <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><i class="wb-search" aria-hidden="true"></i></button>
                          </div>
                        </div>
                      </div>
                    </form>
                    <div class="btn-group">
                        <button type="button" class="btn btn-icon btn-outline btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"
                          aria-hidden="true" style="height: 38px;"><?php echo $rows_per_page; ?> rows per page</button>
                        <div class="dropdown-menu" role="menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -139px, 0px); top: 0px; left: 0px; will-change: transform;">
                          <?php $query = $_GET; $query['rows'] = 10; $query_result = http_build_query($query); ?>
                          <a class="dropdown-item" href="<?php echo $_SERVER['PHP_SELF']; ?>?<?php echo $query_result; ?>" role="menuitem">10 rows per page</a>
                          <?php $query = $_GET; $query['rows'] = 20; $query_result = http_build_query($query); ?>
                          <a class="dropdown-item" href="<?php echo $_SERVER['PHP_SELF']; ?>?<?php echo $query_result; ?>" role="menuitem">20 rows per page</a>
                          <?php $query = $_GET; $query['rows'] = 30; $query_result = http_build_query($query); ?>
                          <a class="dropdown-item" href="<?php echo $_SERVER['PHP_SELF']; ?>?<?php echo $query_result; ?>" role="menuitem">30 rows per page</a>
                          <?php $query = $_GET; $query['rows'] = 50; $query_result = http_build_query($query); ?>
                          <a class="dropdown-item" href="<?php echo $_SERVER['PHP_SELF']; ?>?<?php echo $query_result; ?>" role="menuitem">50 rows per page</a>
                          <?php $query = $_GET; $query['rows'] = 100; $query_result = http_build_query($query); ?>
                          <a class="dropdown-item" href="<?php echo $_SERVER['PHP_SELF']; ?>?<?php echo $query_result; ?>" role="menuitem">100 rows per page</a>
                        </div>
                      </div>

                  </div>
                  <div class="col-md-6 text-right">
                    <span class="label label-default mr-15">
                      <?php echo (($current_page - 1) * $rows_per_page) + 1; ?> to
                      <?php echo ($current_page == $page_count) ? (($current_page * $rows_per_page) - (($current_page * $rows_per_page) - $student_count)) : ($current_page * $rows_per_page); ?> &nbsp; of
                      <?php echo $student_count; ?>
                    </span>
                    <div class="btn-group" aria-label="Default button group" role="group" id="view-all-pagination-btn-group">
                      <button type="button" class="btn btn-outline btn-default"><i class="icon wb-chevron-left-mini" aria-hidden="true"></i></button>
                      <?php for ($x = 1; $x <= $page_count; $x++) { ?>
                      <?php
                      $query = $_GET;
                      $query['page'] = $x;
                      $query_result = http_build_query($query);
                      ?>
                      <a class="btn<?php echo ($current_page == $x) ? ' btn-primary' : ' btn-outline btn-default'; ?>" href="<?php echo $_SERVER['PHP_SELF']; ?>?<?php echo $query_result; ?>"><?php echo $x; ?></a>
                      <?php } ?>
                      <button type="button" class="btn btn-outline btn-default"><i class="icon wb-chevron-right-mini" aria-hidden="true"></i></button>
                    </div>
                  </div>
                </div>
                <iframe name="function-iframe" class="d-none"></iframe>
                <div id="function-iframe" class="d-none"></div>
                <!-- Success Modal -->
                <div class="modal fade" id="successModal" aria-hidden="true" aria-labelledby="successModal"
                  role="dialog" tabindex="-1">
                  <div class="modal-dialog modal-simple">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="exampleModalTitle">Success!</h4>
                      </div>
                      <div class="modal-body">
                        <p class="text-left mb-0">The record is deleted from the database.</p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End Modal -->
                <table class="table table-striped table-bordered toggle-arrow-tiny table-sm text-center" id="exampleFooAccordion" data-toggle-column="last">
                  <thead>
                    <tr>
                      <th class="py-10" data-name="id" data-type="number" data-breakpoints="xs">ID</th>
                      <th class="py-10" data-name="name">Name</th>
                      <th class="py-10" data-name="class" data-breakpoints="xs">Class</th>
                      <th class="py-10" data-name="gender" data-breakpoints="xs sm">Gender</th>
                      <th class="py-10" data-name="birth_date" data-breakpoints="xs sm" data-type="date" data-format-string="MMM DD, YYYY">Date of Birth</th>
                      <th class="py-10" data-name="age" data-breakpoints="xs sm">Age</th>
                      <th class="py-10" data-name="nationality" data-breakpoints="all">Nationality</th>
                      <th class="py-10" data-name="contact_no" data-breakpoints="xs sm">Contact Number</th>
                      <th class="py-10" data-name="address" data-breakpoints="all">Address</th>
                      <th class="py-10" data-name="guardian" data-breakpoints="all">Guardian</th>
                      <th class="py-10" data-name="alternate_person" data-breakpoints="all">Alternate Person to Guardian</th>
                      <th class="py-10" data-name="siblings" data-breakpoints="all">Siblings in School</th>
                      <th class="py-10" data-name="medicine" data-breakpoints="all">Medicines</th>
                      <th class="py-10" data-name="nutrition" data-breakpoints="all">Nutrition</th>
                      <th class="py-10" data-name="status">Actions</th>
                      <th class="py-10" data-name="others">Others</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $sql = "SELECT * FROM `student` ORDER BY id DESC LIMIT " . $rows_per_page . " OFFSET " . (($current_page - 1) * $rows_per_page);
                    $result = mysqli_query($conn, $sql);
                    if (mysqli_num_rows($result) > 0) {
                      while($row = mysqli_fetch_assoc($result)) { ?>
                    <tr>
                      <td><?= $row["id"]; ?></td>
                      <td class="text-left text-capitalize"><?= $row["name_last"] . (($row["name_last"] !== '' && $row["name_first"] !== '') ? ', ' : '') . $row["name_first"] . ' ' . $row["name_middle"]; ?></td>
                      <td><?= ($row["gys"] !== '0') ? $row["gys"] : ''; ?></td>
                      <td><?= $row["gender"]; ?></td>
                      <td><?= $row["birth_date"]; ?></td>
                      <td><?= ($row["age"] !== '0') ? $row["age"] : ''; ?></td>
                      <td><?= $row["nationality"]; ?></td>
                      <td><?= $row["contact_no"]; ?></td>
                      <td><?= $row["address"]; ?></td>
                      <td>
                        <?php
                          echo $row["guardian_name"];
                          echo ($row["guardian_contact_no"] !== "") ? ' &nbsp; <strong>Contact Number:</strong> ' . $row["guardian_contact_no"] : '';
                          echo ($row["guardian_address"] !== "") ? ' &nbsp; <strong>Address:</strong> ' . $row["guardian_address"] : '';
                          echo ($row["guardian_office_contact_no"] !== "") ? ' &nbsp; <strong>Office Contact Number:</strong> ' . $row["guardian_office_contact_no"] : '';
                          echo ($row["guardian_office_address"] !== "") ? ' &nbsp; <strong>Office Address:</strong> ' . $row["guardian_office_address"] : '';
                        ?>
                      </td>
                      <td><?= $row["alternate_person_name"]; ?>
                        <?php echo ($row["alternate_person_contact_no"] !== "") ? ' &nbsp; <strong>Contact Number:</strong> ' . $row["alternate_person_contact_no"] : ''; ?></td>
                      <td>
                        <?php
                        if ($row["sibling_1_name"] !== "" || $row["sibling_1_name"] !== "" || $row["sibling_1_name"] !== "") { 
                          echo ($row["sibling_1_name"] !== "") ? $row["sibling_1_name"] . ' (' . $row["sibling_1_cys"] . ')' : '';
                          echo ($row["sibling_2_name"] !== "") ? ', ' . $row["sibling_2_name"] . ' (' . $row["sibling_2_cys"] . ')' : '';
                          echo ($row["sibling_3_name"] !== "") ? ', ' . $row["sibling_3_name"] . ' (' . $row["sibling_3_cys"] . ')' : '';
                        } else { echo 'No siblings in database'; }
                        ?>
                      </td>
                      <td>
                        <?php
                        $sql3 = "SELECT * FROM `medicine` WHERE student_id=" . $row["id"] . " AND can_be_administered=1";
                        $result3 = mysqli_query($conn, $sql3);
                        if (mysqli_num_rows($result3) > 0) echo '<strong>Yes:</strong>'; {
                          $index3 = 0;
                          while($row3 = mysqli_fetch_assoc($result3)) {
                            echo ($index3 == 0) ? ' ' : ', ';
                            echo $row3["name"];
                            $index3++;
                          }
                        }
                        $sql4 = "SELECT * FROM `medicine` WHERE student_id=" . $row["id"] . " AND can_be_administered=0";
                        $result4 = mysqli_query($conn, $sql4);
                        if (mysqli_num_rows($result4) > 0) echo '<br><strong>No:</strong>'; {
                          $index4 = 0;
                          while($row4 = mysqli_fetch_assoc($result4)) {
                            echo ($index4 == 0) ? ' ' : ', ';
                            echo $row4["name"];
                            $index4++;
                          }
                        } ?>
                      </td>
                      <td class="p-0">
                        <table class="footable-details table table-bordered toggle-arrow-tiny table-sm text-center">
                          <tbody>
                            <tr>
                              <th>Year</th>
                              <th>Weight (kg)</th>
                              <th>Height (cm)</th>
                              <th>BMI</th>
                              <th>Category</th>
                            </tr>
                            <?php
                            $sql2 = "SELECT * FROM `nutrition` WHERE student_id=" . $row["id"];
                            $result2 = mysqli_query($conn, $sql2);
                            if (mysqli_num_rows($result2) > 0) { while($row2 = mysqli_fetch_assoc($result2)) { ?>
                            <tr>
                              <td><?= $row2["year"]; ?></td>
                              <td><?= $row2["weight"]; ?></td>
                              <td><?= $row2["height"]; ?></td>
                              <td><?= $row2["bmi"]; ?></td>
                              <td><?= $row2["bmi_category"]; ?></span></td>
                            </tr>
                            <?php } } else { ?>
                            <tr>
                              <td colspan="999" class="text-center">No records.</td>
                            </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </td>
                      <td>
                        <a href="<?php echo $root_dir; ?>/edit?id=<?php echo $row["id"]; ?>"><i class="icon wb-pencil p-5" style="color: #76838f;" aria-hidden="true"></i></a>
                        <a href="javascript:void(0)"><i class="icon wb-trash p-5" style="color: #76838f;" aria-hidden="true" data-target="#deleteModal-<?php echo $row["id"]; ?>" data-toggle="modal"></i></a>
                          <!-- Modal -->
                          <div class="modal fade" id="deleteModal-<?php echo $row["id"]; ?>" aria-hidden="true" aria-labelledby="deleteModal-<?php echo $row["id"]; ?>"
                            role="dialog" tabindex="-1">
                            <div class="modal-dialog modal-simple">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                  </button>
                                  <h4 class="modal-title" id="exampleModalTitle">Warning!</h4>
                                </div>
                                <div class="modal-body">
                                  <p class="text-left mb-0">This will delete the record in the database. Are you sure about this?</p>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                  <button type="button" class="btn btn-primary delete-record-button" data-dismiss="modal" data-home="<?php echo $root_dir; ?>" data-id="<?php echo $row["id"]; ?>">Confirm</button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- End Modal -->
                        <a href="<?php echo $root_dir; ?>/print?id=<?php echo $row["id"]; ?>" target="function-iframe"><i class="icon wb-print p-5" style="color: #76838f;" aria-hidden="true"></i></a>
                      </td>
                      <td> More</td>
                    </tr><?php } } ?>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- End Panel Accordion -->

          </div>
        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("../footer.php"); ?>
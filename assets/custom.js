$(document).ready(function(){
    if (jQuery) {

        // Header

        // Open Search to Edit Modal
        $('#search-to-edit-nav-btn').on('click', function(e) {
            $('#search-to-edit').modal('show');
            setTimeout(function() { $('#search-to-edit-input').focus(); }, 600);
        });
        // Do ajax search on change
        $('#search-to-edit-input').on('keyup', function(e) {
            $('#livesearch').load( $('#search-to-edit-input').attr('data-home') + '/ajax-search.php?q=' + $( "#search-to-edit-input" ).val() + '&by=' + $('#ajax-search-by').attr('data-value') );
        });
        // Change search-by option
        $('#ajax-search-by-menus a').on('click', function(e) {
            $('#ajax-search-by').attr('data-value', $(this).attr('data-value') );
            $('#ajax-search-by').html( $(this).html() );
            $('#search-to-edit-input').focus();
        });


        // Add Page

        // Add another sibling
        $(document.body).on('click', '#showSiblingFields', function(e) {
            if ( $('#sibling_2').hasClass('d-none') ) $('#sibling_2').removeClass("d-none");
            else if ( $('#sibling_3').hasClass('d-none') ) {
                $('#sibling_3').removeClass("d-none");
                $('#showSiblingFields').addClass('d-none');
            }
        });
        // Add input field for Medicine Names (to pass also the medicine names to database)
        function createMedicineFields() {
            var medicineNamesInput = '<div class="d-none form-group">';
            $('#medicineChecklist .inputMedicineLabel').each(function() {
                medicineNamesInput += '<input type="text" class="form-control" name="label' + ($(this).attr("for")) + '" value="' + ($(this).html()) + '" />';
            });
            medicineNamesInput += '</div>';
            $('#medicineChecklist').append( $(medicineNamesInput) );
        }
        createMedicineFields();
        // Add input field for Nutrition Records (to pass also the BMI Records to database)
        function updateNutritionFields() {
            $('#generatedNutritionFields').remove();
            var nutritionInput = '<div id="generatedNutritionFields" class="d-none form-group">';
            var index = 0;
            $('#nutrition-accordion tr.nutrition-accordion').each(function() {
                var year = $(this).find('td:nth-child(1)').html();
                var bmi = $(this).find('td:nth-child(2)').html();
                var category = $(this).find('td:nth-child(3)').html();
                var weight = $(this).next().find('td:nth-child(2)').html();
                var height = $(this).next().next().find('td:nth-child(2)').html();
                nutritionInput += '<input type="text" class="form-control" name="nutrition_' + index + '_year" value="' + year + '" />';
                nutritionInput += '<input type="text" class="form-control" name="nutrition_' + index + '_bmi" value="' + bmi + '" />';
                nutritionInput += '<input type="text" class="form-control" name="nutrition_' + index + '_category" value="' + category + '" />';
                nutritionInput += '<input type="text" class="form-control" name="nutrition_' + index + '_weight" value="' + weight + '" />';
                nutritionInput += '<input type="text" class="form-control" name="nutrition_' + index + '_height" value="' + height + '" />';
                index++;
            });
            nutritionInput += '</div>';
            $('#medicineChecklist').append( $(nutritionInput) );
        }
        // When user added a weight and height value in BMI List editor
        $('#editableTable').on('change', 'td:nth-child(3)', function(e) {
            var sWeight = $(this).parent().find('td:nth-child(2)').html();
            var sHeight = $(this).parent().find('td:nth-child(3)').html();
            if (sWeight != '' && sHeight != '') {
                var fWeight = parseFloat( sWeight );
                var fHeight = ( parseFloat( sHeight ) ) / 100;
                var fBMI = fWeight / ( fHeight * fHeight );
                var sCategory;
                if (fBMI < 18.5) sCategory = 'Underweight';
                else if (fBMI < 25) sCategory = 'Normal';
                else if (fBMI < 30) sCategory = 'Overweight';
                else sCategory = 'Obese';
                var sBMI = (Math.round(fBMI * 100) / 100).toString();
                var index = $(this).parent().index() + 1;
                $('#editableTable tbody tr:nth-child(' + index + ') th:nth-child(4)').html(sBMI);
                $('#editableTable tbody tr:nth-child(' + index + ') th:nth-child(5)').html(sCategory);
            }
        });
        // When user clicked actions in BMI List editor
        $('#editableTable').on('click', '.wb-plus-circle', function(e) {
            var trelement = $('<tr><td class="year" tabindex="1"></td><td class="weight" tabindex="1"></td><td class="height" tabindex="1"></td><th class="bmi"></th><th class="category"></th><th><span class="icon wb-trash" aria-hidden="true"></span> &nbsp; <span class="icon wb-plus-circle" aria-hidden="true"></span></th></tr>');
            $(this).parent().parent().parent().append(trelement);
        });
        $('#editableTable').on('click', '.wb-trash', function(e) {
            if( $(this).parent().parent().parent().children().length > 1 ) {
                $(this).parent().parent().remove();
            }
            else {
                var trelement = $('<tr><td class="year" tabindex="1"></td><td class="weight" tabindex="1"></td><td class="height" tabindex="1"></td><th class="bmi"></th><th class="category"></th><th><span class="icon wb-trash" aria-hidden="true"></span> &nbsp; <span class="icon wb-plus-circle" aria-hidden="true"></span></th></tr>');
                $(this).parent().parent().parent().append(trelement);
                $(this).parent().parent().remove();
            }
        });
        // When user clicked save edited BMI List
        $(document.body).on('click', '#nutritionSaveChanges', function(e) {
            $('#nutrition-accordion').find('tbody').html('');
            $('#editableTable tbody tr').each(function () {
                var year = $(this).find('.year').html();
                var bmi = $(this).find('.bmi').html();
                var category = $(this).find('.category').html();
                var weight = $(this).find('.weight').html();
                var height = $(this).find('.height').html();
                $('#nutrition-accordion').find('tbody').append( $( '<tr class="nutrition-accordion" style="cursor: pointer;"><td>' + year +
                    '</td><td>' + bmi +
                    '</td><td>' + category +
                    '</td><td><i class="icon wb-chevron-right-mini"></i></td></tr><tr class="d-none"><th colspan="2">Weight (kg)</th><td colspan="2">' + weight +
                    '</td></tr><tr class="d-none"><th colspan="2">Height (cm)</th><td colspan="2">' + height +
                    '</td></tr>' )
                );
            });
            updateNutritionFields();
            $('#exampleNiftySlideFromBottom').modal('hide');
        });
        // When user clicked BMI List item, toggle accordion
        $('#nutrition-accordion').on('click', '.nutrition-accordion', function(e) {
            if ( $(this).find('.icon').hasClass('wb-chevron-right-mini') ) { // if accordion is closed, open it:
                $(this).find('.wb-chevron-right-mini').addClass('wb-chevron-down-mini').removeClass('wb-chevron-right-mini');
                $(this).next().removeClass('d-none');
                $(this).next().next().removeClass('d-none');
            }
            else { // if accordion is opened, close it:
                $(this).find('.wb-chevron-down-mini').addClass('wb-chevron-right-mini').removeClass('wb-chevron-down-mini');
                $(this).next().addClass('d-none');
                $(this).next().next().addClass('d-none');
            }
        });
        // When user wanted to edit BMI List and clicked Edit List
        $(document.body).on('click', '#editBMIList', function(e) {
            $('#editableTable tbody').html('');
            if ( $('#nutrition-accordion .nutrition-accordion td' ).html() == 'No records' ) {
                var trelement = $('<tr><td class="year" tabindex="1">' +
                    '</td><td class="weight" tabindex="1">' +
                    '</td><td class="height" tabindex="1">' +
                    '</td><th class="bmi">' +
                    '</th><th class="category">' +
                    '</th><th><span class="icon wb-trash" aria-hidden="true"></span> &nbsp; <span class="icon wb-plus-circle" aria-hidden="true"></span></th></tr>');
                $('#editableTable tbody').append(trelement);
            } else {
                $('#nutrition-accordion .nutrition-accordion').each(function () {
                    var year = $(this).find('td:nth-child(1)').html();
                    var bmi = $(this).find('td:nth-child(2)').html();
                    var category = $(this).find('td:nth-child(3)').html();
                    var weight = $(this).next().find('td').html();
                    var height = $(this).next().next().find('td').html();
                    var trelement = $('<tr><td class="year" tabindex="1">' + year +
                        '</td><td class="weight" tabindex="1">' + weight +
                        '</td><td class="height" tabindex="1">' + height +
                        '</td><th class="bmi">' + bmi +
                        '</th><th class="category">' + category +
                        '</th><th><span class="icon wb-trash" aria-hidden="true"></span> &nbsp; <span class="icon wb-plus-circle" aria-hidden="true"></span></th></tr>');
                    $('#editableTable tbody').append(trelement);
                });
            }
        });
        // Automatically compute age from date of birth
        $('#inputDateOfBirth').on('change', function(e) {
            var dob = new Date( $(this).val() );
            var today = new Date();
            $('#inputAge').val(Math.floor( (today - dob) / (365.25 * 24 * 60 * 60 * 1000) ));
        });



        // View All Page
        
        // Hide other page numbers in paginations
        var prevButton = $('#view-all-pagination-btn-group > button:first-child');
        var nextButton = $('#view-all-pagination-btn-group > button:last-child');
        var activeButtonIndex = $('#view-all-pagination-btn-group > a.btn.btn-primary').index();
        if ( $('#view-all-pagination-btn-group > a.btn').length > 5 ) {
            var firstPageOfNext = ((Math.floor(activeButtonIndex / 5)) * 5) + 1;
            $('#view-all-pagination-btn-group > a.btn:nth-child(' + (firstPageOfNext + 5) + ')').nextUntil( nextButton ).addClass('d-none');
            $('#view-all-pagination-btn-group > a.btn:nth-child(' + (firstPageOfNext + 1) + ')').prevUntil( prevButton ).addClass('d-none');
        }
        // When previous is clicked
        prevButton.on('click', function(e) {
            var indexOfFirstPageVisible = $('#view-all-pagination-btn-group > a.btn:not(.d-none)').first().index();
            var firstPageVisible = $('#view-all-pagination-btn-group > a.btn:nth-child(' + indexOfFirstPageVisible + ')');
            if ( indexOfFirstPageVisible > 5 ) {
                $('#view-all-pagination-btn-group > a.btn:not(.d-none)').addClass('d-none');
                firstPageVisible.next().prevUntil( firstPageVisible.prev().prev().prev().prev().prev() ).removeClass('d-none');
            }
        });
        // When next is clicked
        nextButton.on('click', function(e) {
            var indexOfLastPageVisible = $('#view-all-pagination-btn-group > a.btn:not(.d-none)').last().index();
            var lastPageVisible = $('#view-all-pagination-btn-group > a.btn:nth-child(' + indexOfLastPageVisible + ')');
            if ( $('#view-all-pagination-btn-group > a.btn').length > indexOfLastPageVisible ) {
                $('#view-all-pagination-btn-group > a.btn:not(.d-none)').addClass('d-none');
                lastPageVisible.next().nextUntil( lastPageVisible.next().next().next().next().next().next().next() ).removeClass('d-none');
            }
        });



        // Import Page
        
        // Import CSV
        $('#uploadCSV').change(function(e) {
            if ((window.FileReader) && (e.target.files != undefined)) {
                var reader = new FileReader(); 
                reader.onload = function(e) {
                    var lineSplit = e.target.result.split("\n");
                    var commaSplit = lineSplit[0].split(",");
                    var content = "";
                    for(var i = 0; i < commaSplit.length; i++) {
                        var temp = commaSplit[i];
                        content = content + " " + temp;
                    }
                    var fileContent = reader.result;
                    alert(fileContent);
                };
                reader.readAsText(e.target.files.item(0));    
            }
        });

        var csvval; // Declared this to be available within functions (globally)
        var csvalheaders; // Declared this to be available within functions (globally)
        // If next button is clicked
        $('#exampleWizardForm .wizard-buttons a[data-wizard="next"]').on('click', function(e) {
            // Check if file has been selected.
            if ($('#input-file-now').val().split('.').pop() == 'csv') { // If correct CSV file
                $('#exampleWizardForm #mapForm .error').addClass('d-none');
                $('#exampleWizardForm #mapForm .success').removeClass('d-none');
                $('#exampleWizardForm .steps .step[data-target="#upload"]').addClass('step-success');
                $('#exampleWizardForm .steps .step[data-target="#upload"]').removeClass('step-error');
                csvalheaders = csvval[0].split(",");
                var element = '';
                for (i = 0; i < $('#database-column-heads li').length; i++) {
                    if(typeof csvalheaders[i] != 'undefined')
                        element += '<li class="list-group-item">' + csvalheaders[i] + ' <i class="icon wb-arrow-right ml-10" aria-hidden="true"></i></li>';
                    else element += '<li class="list-group-item">(No Data) <i class="icon wb-arrow-right ml-10" aria-hidden="true"></i></li>';
                }
                $('#exampleWizardForm #csv-column-heads').html(element);
            }
            else { // If no CSV file was selected
                $('#exampleWizardForm #mapForm .success').addClass('d-none');
                $('#exampleWizardForm #mapForm .error').removeClass('d-none');
                $('#exampleWizardForm .steps .step[data-target="#upload"]').addClass('step-error');
                $('#exampleWizardForm .steps .step[data-target="#upload"]').removeClass('step-success');
            }
            // If next button is clicked and its now in the import stage
            if ( $('#exampleWizardForm .steps .step[data-target="#map"]').delay( 5000 ) ) {
                if ( $('#exampleWizardForm .steps .step[data-target="#map"]').hasClass('active') ) {
                    var importFormElement = '<form method="post" action="import.php" id="import-sql-form" class="d-none">';
                    var databaseColumnHeads = $('#database-column-heads li');
                    var csvStudentData = '';
                    console.log(csvval);
                    for (i = 1; i < csvval.length; i++) {
                        if (csvval[i] != "") {
                            csvStudentData = csvval[i].split(",");
                            for (j = 0; j < databaseColumnHeads.length; j++) {
                                importFormElement += '<input type="text" name="studentData[' + (i - 1) + '][\'' +
                                    $('#database-column-heads li:nth-child(' + (j + 1) + ')').attr('data-key') + '\']" value="' +
                                    csvStudentData[j] + '" />';
                            }
                        }
                    }
                    importFormElement += '<button type="submit">Submit</button></form>';
                    $("#import-sql").html($(importFormElement));
                    console.log();
                    $.post('import.php', $('#import-sql-form').serialize(), function (response) {
                        $('#import-ajax').html(response);
                    });
                }
            }
        });
        // Read files of csv on input change
        $("#input-file-now").change(function(e) {
            if (e.target.files != undefined) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    csvval = e.target.result.split("\n");
                };
                reader.readAsText(e.target.files.item(0));
            }
            return false;
        });



        // View All Page

        // Delete button
        $(document.body).on('click', '.delete-record-button', function(e) {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-home') + '/delete?id=' + id;
            console.log(url);
            console.log(id);
            $( "#function-iframe" ).load( url, function(response, status, xhr) {
                if ( status == "error" )
                    alert( "Sorry but there was an error: " + xhr.status + " " + xhr.statusText );
                else if ( $('#function-iframe > div:last-child').html().substring(0, 7) === 'success' ) {
                    $('#exampleFooAccordion').find('tbody > tr > td:first-child').filter(function() {
                        if ( $(this).text() === $('#function-iframe > div:last-child').html().substring(8) ) {
                            $(this).parent().addClass('d-none');
                            $('#successModal').modal('show');
                            setTimeout(function(){ $('#successModal').modal('hide'); }, 1500); 
                        }
                    });
                }
            });
        });

        // Hide alert after 2 seconds
        $(".alert").delay(1000).fadeOut( 2000, function() {
            $(this).hide();
        });
        $('.alert').mouseover(function () { if($(this).is(':animated')) {
            $(this).stop().animate({opacity:'100'});
        }});

    } else {
      console.log("JQuery can't be loaded.");
    }
 });
-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2018 at 01:31 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `health_rms`
--

-- --------------------------------------------------------

--
-- Table structure for table `medicine`
--

CREATE TABLE `medicine` (
  `id` int(11) NOT NULL,
  `name` varchar(35) DEFAULT NULL,
  `can_be_administered` tinyint(1) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medicine`
--

INSERT INTO `medicine` (`id`, `name`, `can_be_administered`, `student_id`) VALUES
(1, 'Paracetamol', 1, 60),
(2, 'Mefenamic Acid', 1, 60),
(3, 'Bonamine', 1, 60),
(4, 'Kremil S.', 1, 60),
(5, 'Vicks', 1, 60),
(6, 'White Flower', 1, 60),
(7, 'Efficacent Oil', 1, 60),
(8, 'Hydrite', 1, 60),
(9, 'Neozep', 1, 60),
(10, 'Carbocistein', 1, 60),
(11, 'Salbutanol', 1, 60),
(12, 'Loratidine', 1, 60),
(13, 'Desowen', 1, 60),
(14, 'Betadine', 1, 60),
(15, 'Visine', 1, 60),
(16, 'Bactidol', 1, 60),
(17, 'BSI', 1, 60),
(18, 'Flammazine Cream', 1, 60),
(19, 'Cold Compress', 1, 60),
(20, 'Deguadin', 1, 60),
(21, 'Paracetamol', 1, 61),
(22, 'Mefenamic Acid', 1, 61),
(23, 'Bonamine', 1, 61),
(24, 'Kremil S.', 1, 61),
(25, 'Vicks', 1, 61),
(26, 'White Flower', 1, 61),
(27, 'Efficacent Oil', 1, 61),
(28, 'Hydrite', 1, 61),
(29, 'Neozep', 1, 61),
(30, 'Carbocistein', 1, 61),
(31, 'Salbutanol', 1, 61),
(32, 'Loratidine', 1, 61),
(33, 'Desowen', 1, 61),
(34, 'Betadine', 1, 61),
(35, 'Visine', 1, 61),
(36, 'Bactidol', 1, 61),
(37, 'BSI', 1, 61),
(38, 'Flammazine Cream', 1, 61),
(39, 'Cold Compress', 1, 61),
(40, 'Deguadin', 1, 61),
(41, 'Paracetamol', 1, 62),
(42, 'Mefenamic Acid', 1, 62),
(43, 'Bonamine', 1, 62),
(44, 'Kremil S.', 1, 62),
(45, 'Vicks', 1, 62),
(46, 'White Flower', 1, 62),
(47, 'Efficacent Oil', 1, 62),
(48, 'Hydrite', 1, 62),
(49, 'Neozep', 1, 62),
(50, 'Carbocistein', 1, 62),
(51, 'Salbutanol', 1, 62),
(52, 'Loratidine', 1, 62),
(53, 'Desowen', 1, 62),
(54, 'Betadine', 1, 62),
(55, 'Visine', 1, 62),
(56, 'Bactidol', 1, 62),
(57, 'BSI', 1, 62),
(58, 'Flammazine Cream', 1, 62),
(59, 'Cold Compress', 1, 62),
(60, 'Deguadin', 1, 62),
(61, 'Paracetamol', 1, 63),
(62, 'Mefenamic Acid', 1, 63),
(63, 'Bonamine', 1, 63),
(64, 'Kremil S.', 1, 63),
(65, 'Vicks', 1, 63),
(66, 'White Flower', 1, 63),
(67, 'Efficacent Oil', 1, 63),
(68, 'Hydrite', 1, 63),
(69, 'Neozep', 1, 63),
(70, 'Carbocistein', 1, 63),
(71, 'Salbutanol', 1, 63),
(72, 'Loratidine', 1, 63),
(73, 'Desowen', 1, 63),
(74, 'Betadine', 1, 63),
(75, 'Visine', 1, 63),
(76, 'Bactidol', 1, 63),
(77, 'BSI', 1, 63),
(78, 'Flammazine Cream', 1, 63),
(79, 'Cold Compress', 1, 63),
(80, 'Deguadin', 1, 63),
(81, 'Paracetamol', 1, 64),
(82, 'Mefenamic Acid', 1, 64),
(83, 'Bonamine', 1, 64),
(84, 'Kremil S.', 1, 64),
(85, 'Vicks', 1, 64),
(86, 'White Flower', 1, 64),
(87, 'Efficacent Oil', 1, 64),
(88, 'Hydrite', 1, 64),
(89, 'Neozep', 1, 64),
(90, 'Carbocistein', 1, 64),
(91, 'Salbutanol', 1, 64),
(92, 'Loratidine', 1, 64),
(93, 'Desowen', 1, 64),
(94, 'Betadine', 1, 64),
(95, 'Visine', 1, 64),
(96, 'Bactidol', 1, 64),
(97, 'BSI', 1, 64),
(98, 'Flammazine Cream', 1, 64),
(99, 'Cold Compress', 1, 64),
(100, 'Deguadin', 1, 64),
(101, 'Paracetamol', 1, 65),
(102, 'Mefenamic Acid', 1, 65),
(103, 'Bonamine', 1, 65),
(104, 'Kremil S.', 1, 65),
(105, 'Vicks', 1, 65),
(106, 'White Flower', 1, 65),
(107, 'Efficacent Oil', 1, 65),
(108, 'Hydrite', 1, 65),
(109, 'Neozep', 1, 65),
(110, 'Carbocistein', 1, 65),
(111, 'Salbutanol', 1, 65),
(112, 'Loratidine', 1, 65),
(113, 'Desowen', 1, 65),
(114, 'Betadine', 1, 65),
(115, 'Visine', 1, 65),
(116, 'Bactidol', 1, 65),
(117, 'BSI', 1, 65),
(118, 'Flammazine Cream', 1, 65),
(119, 'Cold Compress', 1, 65),
(120, 'Deguadin', 1, 65),
(121, 'Paracetamol', 1, 66),
(122, 'Mefenamic Acid', 1, 66),
(123, 'Bonamine', 1, 66),
(124, 'Kremil S.', 1, 66),
(125, 'Vicks', 1, 66),
(126, 'White Flower', 1, 66),
(127, 'Efficacent Oil', 1, 66),
(128, 'Hydrite', 1, 66),
(129, 'Neozep', 1, 66),
(130, 'Carbocistein', 1, 66),
(131, 'Salbutanol', 1, 66),
(132, 'Loratidine', 1, 66),
(133, 'Desowen', 1, 66),
(134, 'Betadine', 1, 66),
(135, 'Visine', 1, 66),
(136, 'Bactidol', 1, 66),
(137, 'BSI', 1, 66),
(138, 'Flammazine Cream', 1, 66),
(139, 'Cold Compress', 1, 66),
(140, 'Deguadin', 1, 66),
(141, 'Paracetamol', 1, 67),
(142, 'Mefenamic Acid', 1, 67),
(143, 'Bonamine', 1, 67),
(144, 'Kremil S.', 1, 67),
(145, 'Vicks', 1, 67),
(146, 'White Flower', 1, 67),
(147, 'Efficacent Oil', 1, 67),
(148, 'Hydrite', 1, 67),
(149, 'Neozep', 1, 67),
(150, 'Carbocistein', 1, 67),
(151, 'Salbutanol', 1, 67),
(152, 'Loratidine', 1, 67),
(153, 'Desowen', 1, 67),
(154, 'Betadine', 1, 67),
(155, 'Visine', 1, 67),
(156, 'Bactidol', 1, 67),
(157, 'BSI', 1, 67),
(158, 'Flammazine Cream', 1, 67),
(159, 'Cold Compress', 1, 67),
(160, 'Deguadin', 1, 67),
(161, 'Paracetamol', 1, 68),
(162, 'Mefenamic Acid', 1, 68),
(163, 'Bonamine', 1, 68),
(164, 'Kremil S.', 1, 68),
(165, 'Vicks', 1, 68),
(166, 'White Flower', 1, 68),
(167, 'Efficacent Oil', 1, 68),
(168, 'Hydrite', 1, 68),
(169, 'Neozep', 1, 68),
(170, 'Carbocistein', 1, 68),
(171, 'Salbutanol', 1, 68),
(172, 'Loratidine', 1, 68),
(173, 'Desowen', 1, 68),
(174, 'Betadine', 1, 68),
(175, 'Visine', 1, 68),
(176, 'Bactidol', 1, 68),
(177, 'BSI', 1, 68),
(178, 'Flammazine Cream', 1, 68),
(179, 'Cold Compress', 1, 68),
(180, 'Deguadin', 1, 68),
(181, 'Paracetamol', 1, 69),
(182, 'Mefenamic Acid', 1, 69),
(183, 'Bonamine', 1, 69),
(184, 'Kremil S.', 1, 69),
(185, 'Vicks', 1, 69),
(186, 'White Flower', 1, 69),
(187, 'Efficacent Oil', 1, 69),
(188, 'Hydrite', 1, 69),
(189, 'Neozep', 1, 69),
(190, 'Carbocistein', 1, 69),
(191, 'Salbutanol', 1, 69),
(192, 'Loratidine', 1, 69),
(193, 'Desowen', 1, 69),
(194, 'Betadine', 1, 69),
(195, 'Visine', 1, 69),
(196, 'Bactidol', 1, 69),
(197, 'BSI', 1, 69),
(198, 'Flammazine Cream', 1, 69),
(199, 'Cold Compress', 1, 69),
(200, 'Deguadin', 1, 69),
(201, 'Paracetamol', 1, 70),
(202, 'Mefenamic Acid', 1, 70),
(203, 'Bonamine', 1, 70),
(204, 'Kremil S.', 1, 70),
(205, 'Vicks', 1, 70),
(206, 'White Flower', 1, 70),
(207, 'Efficacent Oil', 1, 70),
(208, 'Hydrite', 1, 70),
(209, 'Neozep', 1, 70),
(210, 'Carbocistein', 1, 70),
(211, 'Salbutanol', 1, 70),
(212, 'Loratidine', 1, 70),
(213, 'Desowen', 1, 70),
(214, 'Betadine', 1, 70),
(215, 'Visine', 1, 70),
(216, 'Bactidol', 1, 70),
(217, 'BSI', 1, 70),
(218, 'Flammazine Cream', 1, 70),
(219, 'Cold Compress', 1, 70),
(220, 'Deguadin', 1, 70),
(221, 'Paracetamol', 1, 71),
(222, 'Mefenamic Acid', 1, 71),
(223, 'Bonamine', 1, 71),
(224, 'Kremil S.', 1, 71),
(225, 'Vicks', 1, 71),
(226, 'White Flower', 1, 71),
(227, 'Efficacent Oil', 1, 71),
(228, 'Hydrite', 1, 71),
(229, 'Neozep', 1, 71),
(230, 'Carbocistein', 1, 71),
(231, 'Salbutanol', 1, 71),
(232, 'Loratidine', 1, 71),
(233, 'Desowen', 1, 71),
(234, 'Betadine', 1, 71),
(235, 'Visine', 1, 71),
(236, 'Bactidol', 1, 71),
(237, 'BSI', 1, 71),
(238, 'Flammazine Cream', 1, 71),
(239, 'Cold Compress', 1, 71),
(240, 'Deguadin', 1, 71),
(241, 'Paracetamol', 1, 72),
(242, 'Mefenamic Acid', 1, 72),
(243, 'Bonamine', 1, 72),
(244, 'Kremil S.', 1, 72),
(245, 'Vicks', 1, 72),
(246, 'White Flower', 1, 72),
(247, 'Efficacent Oil', 1, 72),
(248, 'Hydrite', 1, 72),
(249, 'Neozep', 1, 72),
(250, 'Carbocistein', 1, 72),
(251, 'Salbutanol', 1, 72),
(252, 'Loratidine', 1, 72),
(253, 'Desowen', 1, 72),
(254, 'Betadine', 1, 72),
(255, 'Visine', 1, 72),
(256, 'Bactidol', 1, 72),
(257, 'BSI', 1, 72),
(258, 'Flammazine Cream', 1, 72),
(259, 'Cold Compress', 1, 72),
(260, 'Deguadin', 1, 72),
(261, 'Paracetamol', 1, 73),
(262, 'Mefenamic Acid', 1, 73),
(263, 'Bonamine', 1, 73),
(264, 'Kremil S.', 1, 73),
(265, 'Vicks', 1, 73),
(266, 'White Flower', 1, 73),
(267, 'Efficacent Oil', 1, 73),
(268, 'Hydrite', 1, 73),
(269, 'Neozep', 1, 73),
(270, 'Carbocistein', 1, 73),
(271, 'Salbutanol', 1, 73),
(272, 'Loratidine', 1, 73),
(273, 'Desowen', 1, 73),
(274, 'Betadine', 1, 73),
(275, 'Visine', 1, 73),
(276, 'Bactidol', 1, 73),
(277, 'BSI', 1, 73),
(278, 'Flammazine Cream', 1, 73),
(279, 'Cold Compress', 1, 73),
(280, 'Deguadin', 1, 73),
(281, 'Paracetamol', 1, 74),
(282, 'Mefenamic Acid', 1, 74),
(283, 'Bonamine', 1, 74),
(284, 'Kremil S.', 1, 74),
(285, 'Vicks', 1, 74),
(286, 'White Flower', 1, 74),
(287, 'Efficacent Oil', 1, 74),
(288, 'Hydrite', 1, 74),
(289, 'Neozep', 1, 74),
(290, 'Carbocistein', 1, 74),
(291, 'Salbutanol', 1, 74),
(292, 'Loratidine', 1, 74),
(293, 'Desowen', 1, 74),
(294, 'Betadine', 1, 74),
(295, 'Visine', 1, 74),
(296, 'Bactidol', 1, 74),
(297, 'BSI', 1, 74),
(298, 'Flammazine Cream', 1, 74),
(299, 'Cold Compress', 1, 74),
(300, 'Deguadin', 1, 74),
(301, 'Paracetamol', 1, 75),
(302, 'Mefenamic Acid', 1, 75),
(303, 'Bonamine', 1, 75),
(304, 'Kremil S.', 1, 75),
(305, 'Vicks', 1, 75),
(306, 'White Flower', 1, 75),
(307, 'Efficacent Oil', 1, 75),
(308, 'Hydrite', 1, 75),
(309, 'Neozep', 1, 75),
(310, 'Carbocistein', 1, 75),
(311, 'Salbutanol', 1, 75),
(312, 'Loratidine', 1, 75),
(313, 'Desowen', 1, 75),
(314, 'Betadine', 1, 75),
(315, 'Visine', 1, 75),
(316, 'Bactidol', 1, 75),
(317, 'BSI', 1, 75),
(318, 'Flammazine Cream', 1, 75),
(319, 'Cold Compress', 1, 75),
(320, 'Deguadin', 1, 75),
(321, 'Paracetamol', 1, 76),
(322, 'Mefenamic Acid', 1, 76),
(323, 'Bonamine', 1, 76),
(324, 'Kremil S.', 1, 76),
(325, 'Vicks', 1, 76),
(326, 'White Flower', 1, 76),
(327, 'Efficacent Oil', 1, 76),
(328, 'Hydrite', 1, 76),
(329, 'Neozep', 1, 76),
(330, 'Carbocistein', 1, 76),
(331, 'Salbutanol', 1, 76),
(332, 'Loratidine', 1, 76),
(333, 'Desowen', 1, 76),
(334, 'Betadine', 1, 76),
(335, 'Visine', 1, 76),
(336, 'Bactidol', 1, 76),
(337, 'BSI', 1, 76),
(338, 'Flammazine Cream', 1, 76),
(339, 'Cold Compress', 1, 76),
(340, 'Deguadin', 1, 76),
(341, 'Paracetamol', 1, 80),
(342, 'Mefenamic Acid', 1, 80),
(343, 'Bonamine', 1, 80),
(344, 'Kremil S.', 1, 80),
(345, 'Vicks', 1, 80),
(346, 'White Flower', 1, 80),
(347, 'Efficacent Oil', 1, 80),
(348, 'Hydrite', 1, 80),
(349, 'Neozep', 1, 80),
(350, 'Carbocistein', 1, 80),
(351, 'Salbutanol', 1, 80),
(352, 'Loratidine', 1, 80),
(353, 'Desowen', 1, 80),
(354, 'Betadine', 1, 80),
(355, 'Visine', 1, 80),
(356, 'Bactidol', 1, 80),
(357, 'BSI', 1, 80),
(358, 'Flammazine Cream', 1, 80),
(359, 'Cold Compress', 1, 80),
(360, 'Deguadin', 1, 80),
(361, 'Paracetamol', 1, 81),
(362, 'Mefenamic Acid', 1, 81),
(363, 'Bonamine', 1, 81),
(364, 'Kremil S.', 1, 81),
(365, 'Vicks', 1, 81),
(366, 'White Flower', 1, 81),
(367, 'Efficacent Oil', 1, 81),
(368, 'Hydrite', 1, 81),
(369, 'Neozep', 1, 81),
(370, 'Carbocistein', 1, 81),
(371, 'Salbutanol', 1, 81),
(372, 'Loratidine', 1, 81),
(373, 'Desowen', 1, 81),
(374, 'Betadine', 1, 81),
(375, 'Visine', 1, 81),
(376, 'Bactidol', 1, 81),
(377, 'BSI', 1, 81),
(378, 'Flammazine Cream', 1, 81),
(379, 'Cold Compress', 1, 81),
(380, 'Deguadin', 1, 81);

-- --------------------------------------------------------

--
-- Table structure for table `nutrition`
--

CREATE TABLE `nutrition` (
  `id` int(11) NOT NULL,
  `year` int(11) DEFAULT NULL,
  `height` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `bmi` double DEFAULT NULL,
  `bmi_cateory` varchar(35) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nutrition`
--

INSERT INTO `nutrition` (`id`, `year`, `height`, `weight`, `bmi`, `bmi_cateory`, `student_id`) VALUES
(1, 2016, 170, 54, 18.69, 'Normal', 73),
(2, 2017, 171, 55, 18.81, 'Normal', 73),
(3, 2016, 170, 54, 18.69, 'Normal', 74),
(4, 2017, 171, 55, 18.81, 'Normal', 74),
(5, 2016, 170, 54, 18.69, 'Normal', 75),
(6, 2017, 171, 55, 18.81, 'Normal', 75),
(7, 2016, 170, 54, 18.69, 'Normal', 76),
(8, 2017, 171, 55, 18.81, 'Normal', 76),
(9, 2016, 170, 54, 18.69, 'Normal', 80),
(10, 2017, 171, 55, 18.81, 'Normal', 80),
(11, 2016, 170, 54, 18.69, 'Normal', 81),
(12, 2017, 171, 55, 18.81, 'Normal', 81);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `name_last` varchar(35) DEFAULT NULL,
  `name_first` varchar(35) DEFAULT NULL,
  `name_middle` varchar(35) DEFAULT NULL,
  `gender` varchar(35) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `nationality` varchar(35) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `contact_no` varchar(35) DEFAULT NULL,
  `guardian_name` varchar(100) DEFAULT NULL,
  `guardian_contact_no` varchar(35) DEFAULT NULL,
  `guardian_address` varchar(100) DEFAULT NULL,
  `guardian_office_contact_no` varchar(35) DEFAULT NULL,
  `guardian_office_address` varchar(100) DEFAULT NULL,
  `alternate_person_name` varchar(100) DEFAULT NULL,
  `alternate_person_contact_no` varchar(35) DEFAULT NULL,
  `sibling_1_name` varchar(100) DEFAULT NULL,
  `sibling_1_cys` varchar(35) DEFAULT NULL,
  `sibling_2_name` varchar(100) DEFAULT NULL,
  `sibling_2_cys` varchar(35) DEFAULT NULL,
  `sibling_3_name` varchar(100) DEFAULT NULL,
  `sibling_3_cys` varchar(35) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name_last`, `name_first`, `name_middle`, `gender`, `birth_date`, `age`, `nationality`, `address`, `contact_no`, `guardian_name`, `guardian_contact_no`, `guardian_address`, `guardian_office_contact_no`, `guardian_office_address`, `alternate_person_name`, `alternate_person_contact_no`, `sibling_1_name`, `sibling_1_cys`, `sibling_2_name`, `sibling_2_cys`, `sibling_3_name`, `sibling_3_cys`) VALUES
(34, 'Bobihis', 'Reynell', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(35, 'Bobihis', 'Reynell', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(36, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(37, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(38, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(39, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(40, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(41, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(42, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(43, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(44, '', 'Rey', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(45, '', 'Rey', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(46, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(47, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(48, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(49, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(50, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(51, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(52, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(53, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(54, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(55, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(56, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(57, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(58, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(59, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(60, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(61, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(62, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(63, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(64, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(65, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(66, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(67, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(68, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(69, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(70, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(71, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(72, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(73, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(74, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(75, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(76, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(77, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(78, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(79, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(80, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(81, '', '', '', 'M', '1970-01-01', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(50) NOT NULL,
  `username` varchar(35) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(10010, 'adminclinic', '$2y$10$nFz/d9IxQ3OIUnG6Jekekejd2mr97vd0NwlDueE13SGHL10RZTD5e');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `medicine`
--
ALTER TABLE `medicine`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `nutrition`
--
ALTER TABLE `nutrition`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `medicine`
--
ALTER TABLE `medicine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=381;

--
-- AUTO_INCREMENT for table `nutrition`
--
ALTER TABLE `nutrition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10011;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `medicine`
--
ALTER TABLE `medicine`
  ADD CONSTRAINT `medicine_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`);

--
-- Constraints for table `nutrition`
--
ALTER TABLE `nutrition`
  ADD CONSTRAINT `nutrition_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php include("header.php"); ?>

    <!-- Page -->
    <div class="page">
      <div class="page-content container-fluid">
        <div class="row" data-plugin="matchHeight" data-by-row="true">

          <div class="col-md-9">
            <!-- Panel Bar Stacked -->
            <div class="card card-shadow" id="chartBarStacked">
              <div class="card-block p-0 p-30 h-full">
                <!-- Example Line -->
                <div class="example-wrap m-md-0">
                  <h4 class="example-title">Nutrition Records</h4>
                  <p>Graph representing the number of people in each BMI categories per year: </p>
                  <div class="example">
                    <div id="exampleMorrisLine"></div>
                  </div>
                </div>
                <!-- End Example Line -->
              </div>
            </div>
            <!-- End Panel Bar Stacked -->
          </div>

          <div class="col-md-3">
            <div class="card card-block">
              <div class="counter counter-lg">
                <div class="counter-label text-uppercase font-size-16">we have</div>
                <div class="counter-number-group">
                  <?php
                  $result = mysqli_query($conn, "SELECT COUNT(id) AS students FROM student");
                  if (mysqli_num_rows($result) > 0) { while($row = mysqli_fetch_assoc($result)) { $students_count = $row['students']; } }
                  ?>
                  <span class="counter-number"><?php echo $students_count; ?></span>
                </div>
                <div class="counter-label text-uppercase font-size-16">students</div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- End Page -->

<?php include("footer.php"); ?>